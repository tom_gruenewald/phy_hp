package tgr.phymosis.homepage;


import javax.sql.DataSource;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import tgr.phymosis.homepage.config.HomepageDatabaseConfig;

@Configuration
@Import(HomepageDatabaseConfig.class)
@EnableAutoConfiguration
public class WorkerServiceConfig
{
  @Bean
  public DataSource dataSource()
  {
    return new EmbeddedDatabaseBuilder()
      .setType(EmbeddedDatabaseType.H2)
      .addScript("classpath:/sql/001_schema.sql")
      .build();
  }
}
