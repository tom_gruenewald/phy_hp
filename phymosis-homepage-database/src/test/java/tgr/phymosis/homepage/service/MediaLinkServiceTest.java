package tgr.phymosis.homepage.service;


import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import tgr.phymosis.homepage.WorkerServiceConfig;
import tgr.phymosis.homepage.beans.ContentBean;
import tgr.phymosis.homepage.beans.MediaLinkBean;
import tgr.phymosis.homepage.beans.UserBean;
import tgr.phymosis.homepage.beans.enums.ContentType;
import tgr.phymosis.homepage.beans.enums.MediaType;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WorkerServiceConfig.class)
public class MediaLinkServiceTest
{

  @Autowired
  MediaLinkService mediaLinkService;


  @Autowired
  private UserService userService;

  @Autowired
  private ContentService contentService;

  private UserBean userBean;

  private static int userNumber;
  private static Date date;
  private static ContentBean contentBean;

  private static final String TITLE = "TestContent";
  private static final String TEXT = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna"
                                     + " aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,"
                                     + " no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed"
                                     + " diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam"
                                     + " et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
  private static final ContentType CONTENT_TYPE = ContentType.OTHER;
  private static final MediaType MEDIA_TYPE = MediaType.NONE;

  @BeforeClass
  public static void before()
  {
    userNumber = 0;
    date = new Date();
  }

  @Before
  public void beforeEach()
  {
    this.userBean = this.userService.createUser("User " + userNumber);
    userNumber++;
    contentBean = this.contentService.createContent(this.userBean.getId(),
                                                    CONTENT_TYPE,
                                                    MEDIA_TYPE,
                                                    date,
                                                    TITLE,
                                                    TEXT);
    assertNotNull(contentBean);
    assertNotNull(contentBean.getId());
  }

  @Test
  @Transactional
  public void createMediaLinkTest()
  {
    MediaLinkBean mediaLinkBean = this.mediaLinkService.createMediaLink(contentBean.getId(),
                                                                        "medialink1");
    assertNotNull(mediaLinkBean);
    assertNotNull(mediaLinkBean.getId());
    assertNotNull(mediaLinkBean.getContentId());
    assertEquals(contentBean.getId(), mediaLinkBean.getContentId());
    assertNotNull(mediaLinkBean.getMediaLink());
    assertEquals("medialink1", mediaLinkBean.getMediaLink());
  }

  @Test
  @Transactional
  public void getMediaLinkByIdTest()
  {
    MediaLinkBean mediaLinkBean = this.mediaLinkService.createMediaLink(contentBean.getId(),
                                                                        "medialink1");
    assertNotNull(mediaLinkBean);
    assertEquals(mediaLinkBean, this.mediaLinkService.getMediaLinkById(mediaLinkBean.getId()));
  }

  @Test
  @Transactional
  public void getAllMediaLinksTest()
  {
    MediaLinkBean mediaLinkBean = this.mediaLinkService.createMediaLink(contentBean.getId(),
                                                                        "medialink1");
    MediaLinkBean mediaLinkBean2 = this.mediaLinkService.createMediaLink(contentBean.getId(),
                                                                        "medialink2");
    assertNotNull(mediaLinkBean);
    assertNotNull(mediaLinkBean2);

    List<MediaLinkBean> fetchedMediaLinks = this.mediaLinkService.getAllMediaLinks();

    assertTrue(fetchedMediaLinks.size() > 1);
    assertTrue(fetchedMediaLinks.contains(mediaLinkBean));
    assertTrue(fetchedMediaLinks.contains(mediaLinkBean2));
  }

  @Test
  @Transactional
  public void getMediaLinksByContentIdTest()
  {

    MediaLinkBean mediaLinkBean = this.mediaLinkService.createMediaLink(contentBean.getId(),
                                                                        "medialink1");
    MediaLinkBean mediaLinkBean2 = this.mediaLinkService.createMediaLink(9999L,
                                                                         "medialink2");
    assertNotNull(mediaLinkBean);
    assertNotNull(mediaLinkBean2);

    List<MediaLinkBean> fetchedMediaLinks = this.mediaLinkService.getMediaLinksByContentId(9999L);

    assertEquals(1, fetchedMediaLinks.size());
    assertTrue(fetchedMediaLinks.contains(mediaLinkBean2));
    assertFalse(fetchedMediaLinks.contains(mediaLinkBean));
  }

  @Test
  @Transactional
  public void deleteMediaLinkByIdTest()
  {

    MediaLinkBean mediaLinkBean = this.mediaLinkService.createMediaLink(999L,
                                                                        "medialink1");
    MediaLinkBean mediaLinkBean2 = this.mediaLinkService.createMediaLink(999L,
                                                                         "medialink2");
    assertNotNull(mediaLinkBean);
    assertNotNull(mediaLinkBean2);

    List<MediaLinkBean> fetchedMediaLinks = this.mediaLinkService.getMediaLinksByContentId(999L);

    assertEquals(2, fetchedMediaLinks.size());
    assertTrue(fetchedMediaLinks.contains(mediaLinkBean));
    assertTrue(fetchedMediaLinks.contains(mediaLinkBean2));

    assertEquals(mediaLinkBean, this.mediaLinkService.deleteMediaLinkById(mediaLinkBean.getId()));

    fetchedMediaLinks = this.mediaLinkService.getMediaLinksByContentId(999L);

    assertEquals(1, fetchedMediaLinks.size());
    assertFalse(fetchedMediaLinks.contains(mediaLinkBean));
    assertTrue(fetchedMediaLinks.contains(mediaLinkBean2));

    assertEquals(mediaLinkBean2, this.mediaLinkService.deleteMediaLinkById(mediaLinkBean2.getId()));

    fetchedMediaLinks = this.mediaLinkService.getMediaLinksByContentId(999L);

    assertEquals(0, fetchedMediaLinks.size());
  }

  @Test
  @Transactional
  public void deleteMediaLinksByContentIdTest()
  {

    MediaLinkBean mediaLinkBean = this.mediaLinkService.createMediaLink(999L,
                                                                        "medialink1");
    MediaLinkBean mediaLinkBean2 = this.mediaLinkService.createMediaLink(999L,
                                                                         "medialink2");
    assertNotNull(mediaLinkBean);
    assertNotNull(mediaLinkBean2);

    List<MediaLinkBean> fetchedMediaLinks = this.mediaLinkService.getMediaLinksByContentId(999L);

    assertEquals(2, fetchedMediaLinks.size());
    assertTrue(fetchedMediaLinks.contains(mediaLinkBean));
    assertTrue(fetchedMediaLinks.contains(mediaLinkBean2));

    assertEquals(fetchedMediaLinks, this.mediaLinkService.deleteMediaLinksByContentId(999L));

    fetchedMediaLinks = this.mediaLinkService.getMediaLinksByContentId(999L);

    assertEquals(0, fetchedMediaLinks.size());
  }
}
