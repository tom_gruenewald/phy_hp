package tgr.phymosis.homepage.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import tgr.phymosis.homepage.WorkerServiceConfig;
import tgr.phymosis.homepage.beans.TourdateBean;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WorkerServiceConfig.class)
public class TourdateServiceTest
{

  @Autowired
  TourdateService tourdateService;

  @Test
  @Transactional
  public void createTourdateTest()
  {
    String location = "Location";
    Date date = new Date();
    String title = "title";
    String googleMapsLink = "googleMapsLink";
    String text = "googleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLink";

    TourdateBean tourdateBean = this.tourdateService.createTourdate(location,
                                                                    date,
                                                                    title,
                                                                    googleMapsLink,
                                                                    text);
    assertNotNull(tourdateBean);
    assertNotNull(tourdateBean.getId());
    assertEquals(location, tourdateBean.getLocation());
    assertEquals(date, tourdateBean.getDate());
    assertEquals(title, tourdateBean.getTitle());
    assertEquals(googleMapsLink, tourdateBean.getGoogleMapsLink());
    assertEquals(text, tourdateBean.getText());
  }

  @Test
  @Transactional
  public void getTourdateByIdTest()
  {
    String location = "Location";
    Date date = new Date();
    String title = "title";
    String googleMapsLink = "googleMapsLink";
    String text = "googleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLink";

    TourdateBean tourdateBean = this.tourdateService.createTourdate(location,
                                                                    date,
                                                                    title,
                                                                    googleMapsLink,
                                                                    text);
    assertNotNull(tourdateBean);
    assertEquals(tourdateBean, this.tourdateService.getTourdateById(tourdateBean.getId()));
  }

  @Test
  @Transactional
  public void getAllTourdatesTest()
  {

    String location = "Location";
    Date date = new Date();
    String title = "title";
    String googleMapsLink = "googleMapsLink";
    String text = "googleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLink";

    TourdateBean tourdateBean = this.tourdateService.createTourdate(location,
                                                                    date,
                                                                    title,
                                                                    googleMapsLink,
                                                                    text);
    assertNotNull(tourdateBean);
    TourdateBean tourdateBean2 = this.tourdateService.createTourdate(location,
                                                                     date,
                                                                     title,
                                                                     googleMapsLink,
                                                                     text);

    assertNotNull(tourdateBean2);

    List<TourdateBean> fetchedList = this.tourdateService.getAllTourdates();
    assertNotNull(fetchedList);
    assertTrue(fetchedList.contains(tourdateBean));
    assertTrue(fetchedList.contains(tourdateBean2));
  }

  @Test
  @Transactional
  public void getAllTourdatesPageTest()
  {
    String location = "Location";
    Date date = new Date();
    String title = "title";
    String googleMapsLink = "googleMapsLink";
    String text = "googleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLink";

    TourdateBean tourdateBean = this.tourdateService.createTourdate(location,
                                                                    date,
                                                                    title,
                                                                    googleMapsLink,
                                                                    text);
    assertNotNull(tourdateBean);
    TourdateBean tourdateBean2 = this.tourdateService.createTourdate(location,
                                                                     new Date(date.getTime() + 86400),
                                                                     title,
                                                                     googleMapsLink,
                                                                     text);

    assertNotNull(tourdateBean2);
    TourdateBean tourdateBean3 = this.tourdateService.createTourdate(location,
                                                                     new Date(date.getTime() + 172800),
                                                                     title,
                                                                     googleMapsLink,
                                                                     text);

    assertNotNull(tourdateBean3);

    List<TourdateBean> fetchedList = this.tourdateService.getTourdatesPage(0, 2);
    assertNotNull(fetchedList);
    assertTrue(fetchedList.contains(tourdateBean));
    assertTrue(fetchedList.contains(tourdateBean2));
    assertFalse(fetchedList.contains(tourdateBean3));
  }

  @Test
  @Transactional
  public void deactivateTourdateByIdTest()
  {

    String location = "Location";
    Date date = new Date();
    String title = "title";
    String googleMapsLink = "googleMapsLink";
    String text = "googleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLink";

    TourdateBean tourdateBean = this.tourdateService.createTourdate(location,
                                                                    date,
                                                                    title,
                                                                    googleMapsLink,
                                                                    text);
    assertNotNull(tourdateBean);
    assertTrue(tourdateBean.getActive());

    assertTrue(this.tourdateService.deactivateTourdateById(tourdateBean.getId()));
    assertFalse(this.tourdateService.getTourdateById(tourdateBean.getId()).getActive());
  }


  @Test
  @Transactional
  public void reactivateTourdateByIdTest()
  {

    String location = "Location";
    Date date = new Date();
    String title = "title";
    String googleMapsLink = "googleMapsLink";
    String text = "googleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLink";

    TourdateBean tourdateBean = this.tourdateService.createTourdate(location,
                                                                    date,
                                                                    title,
                                                                    googleMapsLink,
                                                                    text);
    assertNotNull(tourdateBean);
    assertTrue(tourdateBean.getActive());

    assertTrue(this.tourdateService.deactivateTourdateById(tourdateBean.getId()));
    assertFalse(this.tourdateService.getTourdateById(tourdateBean.getId()).getActive());

    assertEquals(tourdateBean, this.tourdateService.reactivateTourdateById(tourdateBean.getId()));
    assertTrue(this.tourdateService.getTourdateById(tourdateBean.getId()).getActive());
  }

  @Test
  @Transactional
  public void getActiveTourdatesTest()
  {
    String location = "Location";
    Date date = new Date();
    String title = "title";
    String googleMapsLink = "googleMapsLink";
    String text = "googleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLink";

    TourdateBean tourdateBean = this.tourdateService.createTourdate(location,
                                                                    date,
                                                                    title,
                                                                    googleMapsLink,
                                                                    text);
    assertNotNull(tourdateBean);
    TourdateBean tourdateBean2 = this.tourdateService.createTourdate(location,
                                                                     new Date(date.getTime() + 86400),
                                                                     title,
                                                                     googleMapsLink,
                                                                     text);

    assertNotNull(tourdateBean2);
    TourdateBean tourdateBean3 = this.tourdateService.createTourdate(location,
                                                                     new Date(date.getTime() + 172800),
                                                                     title,
                                                                     googleMapsLink,
                                                                     text);

    assertNotNull(tourdateBean3);
    this.tourdateService.deactivateTourdateById(tourdateBean3.getId());

    List<TourdateBean> fetchedList = this.tourdateService.getAllActiveTourdates();
    assertNotNull(fetchedList);
    assertTrue(fetchedList.contains(tourdateBean));
    assertTrue(fetchedList.contains(tourdateBean2));
    assertFalse(fetchedList.contains(tourdateBean3));

  }

  @Test
  @Transactional
  public void getActiveTourdatesPageTest()
  {

    String location = "Location";
    Date date = new Date();
    String title = "title";
    String googleMapsLink = "googleMapsLink";
    String text = "googleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLinkgoogleMapsLink";

    TourdateBean tourdateBean = this.tourdateService.createTourdate(location,
                                                                    date,
                                                                    title,
                                                                    googleMapsLink,
                                                                    text);
    assertNotNull(tourdateBean);
    TourdateBean tourdateBean2 = this.tourdateService.createTourdate(location,
                                                                     new Date(date.getTime() + 86400),
                                                                     title,
                                                                     googleMapsLink,
                                                                     text);

    assertNotNull(tourdateBean2);
    TourdateBean tourdateBean3 = this.tourdateService.createTourdate(location,
                                                                     new Date(date.getTime() + 172800),
                                                                     title,
                                                                     googleMapsLink,
                                                                     text);

    assertNotNull(tourdateBean3);
    TourdateBean tourdateBean4 = this.tourdateService.createTourdate(location,
                                                                     new Date(date.getTime() + 259200),
                                                                     title,
                                                                     googleMapsLink,
                                                                     text);

    assertNotNull(tourdateBean4);
    TourdateBean tourdateBean5 = this.tourdateService.createTourdate(location,
                                                                     new Date(date.getTime() + 345600),
                                                                     title,
                                                                     googleMapsLink,
                                                                     text);

    assertNotNull(tourdateBean5);
    this.tourdateService.deactivateTourdateById(tourdateBean2.getId());
    this.tourdateService.deactivateTourdateById(tourdateBean3.getId());

    List<TourdateBean> fetchedList = this.tourdateService.getActiveTourdatesPage(0, 2);
    assertNotNull(fetchedList);
    assertTrue(fetchedList.contains(tourdateBean));
    assertTrue(fetchedList.contains(tourdateBean4));
    assertFalse(fetchedList.contains(tourdateBean2));
    assertFalse(fetchedList.contains(tourdateBean3));
    assertFalse(fetchedList.contains(tourdateBean5));

  }

}

