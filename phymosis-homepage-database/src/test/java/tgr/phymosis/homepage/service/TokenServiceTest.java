package tgr.phymosis.homepage.service;



import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import tgr.phymosis.homepage.WorkerServiceConfig;
import tgr.phymosis.homepage.beans.TokenBean;
import tgr.phymosis.homepage.beans.UserBean;

import java.util.Date;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WorkerServiceConfig.class)
public class TokenServiceTest {
  @Autowired
  private UserService userService;

  @Autowired
  private TokenService tokenService;

  private UserBean userBean;

  private static int userNumber;

  private static final String PASSWORD = "PASSWORD";

  @BeforeClass
  public static void before()
  {
    userNumber = 0;
  }

  @Before
  public void beforeEach()
  {
    this.userBean = this.userService.createUser("User " + userNumber);
    userNumber++;
  }

  @Test
  @Transactional
  public void createTokenTest()
  {
    assertTrue(this.userService.setUserPassword(this.userBean.getId(), PASSWORD, null));
    TokenBean tokenBean = this.tokenService.createToken(this.userBean.getName(), PASSWORD);
    assertNotNull(tokenBean);
    assertTrue(tokenBean.getToken().contains("z" + this.userBean.getId() + "0"));
    assertNotNull(tokenBean.getExpirationTime());
  }

  @Test
  @Transactional
  public void checkTokenTest()
  {
    assertTrue(this.userService.setUserPassword(this.userBean.getId(), PASSWORD, null));
    assertTrue(this.userService.setUserPassword(this.userBean.getId(), PASSWORD, null));
    TokenBean tokenBean = this.tokenService.createToken(this.userBean.getName(), PASSWORD);
    assertNotNull(tokenBean);
    assertTrue(tokenBean.getToken().contains("z" + this.userBean.getId() + "0"));
    assertNotNull(tokenBean.getExpirationTime());
    assertTrue(this.tokenService.checkToken(tokenBean));
  }
}

