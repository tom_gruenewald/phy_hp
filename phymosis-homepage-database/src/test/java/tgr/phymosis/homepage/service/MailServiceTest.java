package tgr.phymosis.homepage.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import tgr.phymosis.homepage.WorkerServiceConfig;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WorkerServiceConfig.class)
public class MailServiceTest {

  @Autowired
  private MailService mailService;

  @Test
  public void sendEmailTest() {
    this.mailService.sendEmail("phymosis-homepage-notifier@gmx.de",
                               "phymosis@web.de",
                               "notification test",
                               "Dies ist eine Testnachricht des automatischen Notification-Systems der Phymosis Homepage. \n"
                               + "Es dient dem Versenden von Fehlerberichten, beispielsweise beim Absturz der Homepage oder auch Booking-Anfragen....");
  }

}
