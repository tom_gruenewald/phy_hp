package tgr.phymosis.homepage.service;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import tgr.phymosis.homepage.WorkerServiceConfig;
import tgr.phymosis.homepage.beans.ContentBean;
import tgr.phymosis.homepage.beans.UserBean;
import tgr.phymosis.homepage.beans.enums.ContentOrderType;
import tgr.phymosis.homepage.beans.enums.ContentType;
import tgr.phymosis.homepage.beans.enums.MediaType;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WorkerServiceConfig.class)
public class ContentServiceTest
{

  private static final Logger LOGGER = LoggerFactory.getLogger(ContentServiceTest.class);

  @Autowired
  private UserService userService;

  @Autowired
  private ContentService contentService;

  private UserBean userBean;

  private static int userNumber;
  private static Date date;

  private static final String TITLE = "TestContent";
  private static final String TEXT = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna"
                                     + " aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren,"
                                     + " no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed"
                                     + " diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam"
                                     + " et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.";
  private static final ContentType CONTENT_TYPE = ContentType.OTHER;
  private static final MediaType MEDIA_TYPE = MediaType.NONE;

  @BeforeClass
  public static void before()
  {
    userNumber = 0;
    date = new Date();
  }

  @Before
  public void beforeEach()
  {
    this.userBean = this.userService.createUser("User " + userNumber);
    userNumber++;
  }

  @Test
  @Transactional
  public void createContentTest()
  {
    ContentBean contentBean = this.contentService.createContent(this.userBean.getId(),
                                                                CONTENT_TYPE,
                                                                MEDIA_TYPE,
                                                                date,
                                                                TITLE,
                                                                TEXT);
    assertNotNull(contentBean);
    assertNotNull(contentBean.getId());

    assertEquals(this.userBean.getId(), contentBean.getAuthorId());
    assertEquals(CONTENT_TYPE, contentBean.getContentType());
    assertEquals(MEDIA_TYPE, contentBean.getMediaType());
    assertEquals(date, contentBean.getCreationDate());
    assertEquals(TITLE, contentBean.getTitle());
    assertEquals(TEXT, contentBean.getText());
  }

  @Test
  @Transactional
  public void getContentByIdTest()
  {
    ContentBean contentBean1 = this.contentService.createContent(this.userBean.getId(),
                                                                 CONTENT_TYPE,
                                                                 MEDIA_TYPE,
                                                                 date,
                                                                 TITLE,
                                                                 TEXT);

    assertNotNull(contentBean1);
    assertNotNull(contentBean1.getId());

    ContentBean contentBean2 = this.contentService.getContentById(contentBean1.getId());

    assertNotNull(contentBean2);
    assertEquals(contentBean1, contentBean2);
  }

  @Test
  @Transactional
  public void getAllContentsTest()
  {
    ContentBean contentBean1 = this.contentService.createContent(this.userBean.getId(),
                                                                 CONTENT_TYPE,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title1",
                                                                 TEXT);
    assertNotNull(contentBean1);

    ContentBean contentBean2 = this.contentService.createContent(this.userBean.getId(),
                                                                 CONTENT_TYPE,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title2",
                                                                 TEXT);
    assertNotNull(contentBean2);

    ContentBean contentBean3 = this.contentService.createContent(this.userBean.getId(),
                                                                 CONTENT_TYPE,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title3",
                                                                 TEXT);
    assertNotNull(contentBean3);

    List<ContentBean> content = this.contentService.getAllContent();
    assert content != null;
    assertTrue(content.contains(contentBean1));
    assertTrue(content.contains(contentBean2));
    assertTrue(content.contains(contentBean3));
  }

  @Test
  @Transactional
  public void getActiveContentByTypeTest()
  {
    ContentBean contentBean1 = this.contentService.createContent(this.userBean.getId(),
                                                                 ContentType.BAND_MEMBER,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title1",
                                                                 TEXT);
    assertNotNull(contentBean1);

    ContentBean contentBean2 = this.contentService.createContent(this.userBean.getId(),
                                                                 ContentType.OTHER,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title2",
                                                                 TEXT);
    assertNotNull(contentBean2);

    ContentBean contentBean3 = this.contentService.createContent(this.userBean.getId(),
                                                                 ContentType.BAND_MEMBER,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title3",
                                                                 TEXT);
    assertNotNull(contentBean3);

    ContentBean contentBean4 = this.contentService.createContent(this.userBean.getId(),
                                                                 ContentType.BAND_MEMBER,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title4",
                                                                 TEXT);
    assertNotNull(contentBean4);

    List<ContentBean> content = this.contentService.getActiveContentByType(ContentType.BAND_MEMBER,
                                                                           ContentOrderType.TITLE_DESC);
    assertNotNull(content);
    assertEquals(contentBean4, content.get(0));
    assertEquals(contentBean3, content.get(1));
    assertEquals(contentBean1, content.get(2));
    assertFalse(content.contains(contentBean2));
  }

  @Test
  @Transactional
  public void getActiveContentPageableByTypeTest()
  {
    ContentBean contentBean1 = this.contentService.createContent(this.userBean.getId(),
                                                                 ContentType.BAND_MEMBER,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title1",
                                                                 TEXT);
    assertNotNull(contentBean1);

    ContentBean contentBean2 = this.contentService.createContent(this.userBean.getId(),
                                                                 ContentType.OTHER,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title2",
                                                                 TEXT);
    assertNotNull(contentBean2);

    ContentBean contentBean3 = this.contentService.createContent(this.userBean.getId(),
                                                                 ContentType.BAND_MEMBER,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title3",
                                                                 TEXT);
    assertNotNull(contentBean3);

    ContentBean contentBean4 = this.contentService.createContent(this.userBean.getId(),
                                                                 ContentType.BAND_MEMBER,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title4",
                                                                 TEXT);
    assertNotNull(contentBean4);

    List<ContentBean> content = this.contentService.getContentPageableByType(ContentType.BAND_MEMBER,
                                                                             0,
                                                                             2,
                                                                             ContentOrderType.TITLE_ASC,
                                                                             true);
    assertNotNull(content);
    assertEquals(contentBean1, content.get(0));
    assertEquals(contentBean3, content.get(1));
    assertFalse(content.contains(contentBean2));
    assertFalse(content.contains(contentBean4));
  }

  @Test
  @Transactional
  public void deactivateContentByIdTest()
  {
    ContentBean contentBean1 = this.contentService.createContent(this.userBean.getId(),
                                                                 ContentType.BAND_MEMBER,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title1",
                                                                 TEXT);
    assertNotNull(contentBean1);

    this.contentService.deactivateContentById(contentBean1.getId());
    contentBean1 = this.contentService.getContentById(contentBean1.getId());
    assertNotNull(contentBean1);
    assertNotNull(contentBean1.getActive());
    assertFalse(contentBean1.getActive());
  }

  @Test
  @Transactional
  public void reactivateContentByIdTest()
  {
    ContentBean contentBean1 = this.contentService.createContent(this.userBean.getId(),
                                                                 ContentType.BAND_MEMBER,
                                                                 MEDIA_TYPE,
                                                                 new Date(),
                                                                 "Title1",
                                                                 TEXT);
    assertNotNull(contentBean1);

    this.contentService.deactivateContentById(contentBean1.getId());
    contentBean1 = this.contentService.getContentById(contentBean1.getId());
    assertNotNull(contentBean1);
    assertNotNull(contentBean1.getActive());
    assertFalse(contentBean1.getActive());

    ContentBean contentBean2 = this.contentService.reactivateContentById(contentBean1.getId());
    contentBean1 = this.contentService.getContentById(contentBean1.getId());
    assertNotNull(contentBean1);
    assertNotNull(contentBean1.getActive());
    assertTrue(contentBean1.getActive());
    assertNotNull(contentBean2);
    assertNotNull(contentBean2.getActive());
    assertTrue(contentBean2.getActive());
  }

}
