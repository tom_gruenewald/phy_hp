package tgr.phymosis.homepage.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import tgr.phymosis.homepage.WorkerServiceConfig;
import tgr.phymosis.homepage.beans.UserBean;


import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = WorkerServiceConfig.class)
public class UserServiceTest {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceTest.class);

  @Autowired
  private UserService userService;

  @Test
  @Transactional
  public void createUserTest()
  {
    String name = "createUserTestUser1";
    UserBean user = this.userService.createUser(name);
    assertNotNull(user);
    assertEquals(name, user.getName());
    assertNotNull(user.getSecurityLevel());
    assertTrue(user.getActive());
  }

  @Test
  @Transactional
  public void getActiveUserByIdTest()
  {
    String name = "getActiveUserByIdTestUser1";
    UserBean user1 = this.userService.createUser(name);
    assert user1 != null;
    UserBean user2 = this.userService.getActiveUserById(user1.getId());
    assertNotNull(user2);
    assertEquals(user1, user2);
  }

  @Test
  @Transactional
  public void deactivateUserByIdTest()
  {
    String name1 = "deactivateUserByIdTestUser1";
    UserBean user1 = this.userService.createUser(name1);
    assertNotNull(user1);
    assertTrue(user1.getActive());

    String name2 = "deactivateUserByIdTestUser2";
    UserBean user2 = this.userService.createUser(name2);
    assertNotNull(user2);
    assertTrue(user2.getActive());

    String name3 = "deactivateUserByIdTestUser3";
    UserBean user3 = this.userService.createUser(name3);
    assertNotNull(user3);
    assertTrue(user3.getActive());

    this.userService.deactivateUserById(user1.getId());
    assertFalse(Objects.requireNonNull(this.userService.getUserById(user1.getId())).getActive());

    List<Long> ids = Arrays.asList(user2.getId(), user3.getId());
    this.userService.deactivateUserById(ids);

    assertFalse(Objects.requireNonNull(this.userService.getUserById(user2.getId())).getActive());
    assertFalse(Objects.requireNonNull(this.userService.getUserById(user3.getId())).getActive());
  }

  @Test
  @Transactional
  public void getAllUsersTest()
  {
    String name1 = "getAllUsersTestUser1";
    UserBean user1 = this.userService.createUser(name1);
    assert user1 != null;
    String name2 = "getAllUsersTestUser2";
    UserBean user2 = this.userService.createUser(name2);
    assert user2 != null;
    List<UserBean> userList = userService.getAllUsers();
    assert userList != null;
    assertTrue(userList.contains(user1));
    assertTrue(userList.contains(user2));
  }

  @Test
  @Transactional
  public void getUsersByPageTest()
  {
    String name1 = "getUsersByPageTestUser1";
    UserBean user1 = this.userService.createUser(name1);
    assertNotNull(user1);

    String name2 = "getUsersByPageTestUser2";
    UserBean user2 = this.userService.createUser(name2);
    assertNotNull(user2);
    this.userService.deactivateUserById(user2.getId());

    String name3 = "getUsersByPageTestUser3";
    UserBean user3 = this.userService.createUser(name3);
    assertNotNull(user3);

    List<UserBean> fetchedUserBeans1 = this.userService.getUsersByPage(0, 2, false);
    assertNotNull(fetchedUserBeans1);
    assertEquals(2, fetchedUserBeans1.size());
    assertTrue(fetchedUserBeans1.contains(user1) &&
               fetchedUserBeans1.contains(user3));

    List<UserBean> fetchedUserBeans2 = this.userService.getUsersByPage(0, 2, true);
    assertNotNull(fetchedUserBeans2);
    assertEquals(2, fetchedUserBeans2.size());
    assertTrue(fetchedUserBeans2.contains(user1) &&
               fetchedUserBeans2.contains(user2));
  }

  @Test
  @Transactional
  public void setUserPasswordTest()
  {
    String name1 = "setUserPasswordTestUser1";
    UserBean user1 = this.userService.createUser(name1);
    assertNotNull(user1);

    assertTrue(this.userService.setUserPassword(user1.getId(), "xxxx", null));
    assertTrue(this.userService.setUserPassword(user1.getId(), "yyyy", "xxxx"));

    assertFalse(this.userService.setUserPassword(user1.getId(), "zzzz", "zzzz"));
  }

}
