CREATE TABLE PHP_USER
(
    ID        BIGINT PRIMARY KEY,
    NAME      VARCHAR(30) NOT NULL,
    PASSWORD  VARCHAR(128),
    SEC_LEVEL VARCHAR(16),
    ACTIVE    BOOLEAN     NOT NULL
);

CREATE TABLE PHP_TOKEN
(
    ID        BIGINT PRIMARY KEY,
    USER_ID   BIGINT       NOT NULL,
    TOKEN     VARCHAR(128) NOT NULL,
    VALIDATED TIMESTAMP    NOT NULL
);

CREATE TABLE PHP_CONTENT
(
    ID            BIGINT PRIMARY KEY,
    AUTHOR_ID     BIGINT      NOT NULL,
    CONTENT_TYPE  VARCHAR(16) NOT NULL,
    MEDIA_TYPE    VARCHAR(16),
    CREATION_DATE TIMESTAMP   NOT NULL,
    TITLE         VARCHAR(512),
    TEXT          VARCHAR(8192),
    ACTIVE        BOOLEAN     NOT NULL
);

CREATE TABLE PHP_MEDIA_LINK
(
    ID         BIGINT PRIMARY KEY,
    CONTENT_ID BIGINT        NOT NULL,
    MEDIA_LINK VARCHAR(2048) NOT NULL,
    ACTIVE     BOOLEAN       NOT NULL
);

CREATE TABLE PHP_EMAIL_ADDRESS
(
    ID            BIGINT PRIMARY KEY,
    USER_ID       BIGINT        NOT NULL,
    EMAIL_ADDRESS VARCHAR(2048) NOT NULL,
    ACTIVE        BOOLEAN       NOT NULL
);

CREATE TABLE PHP_TOURDATE
(
    ID               BIGINT PRIMARY KEY,
    LOCATION         VARCHAR,
    DATE             TIMESTAMP NOT NULL,
    TITLE            VARCHAR   NOT NULL,
    GOOGLE_MAPS_LINK VARCHAR,
    TEXT             VARCHAR,
    ACTIVE           BOOLEAN   NOT NULL
);

CREATE TABLE PHP_SOCIAL_MEDIA_LINK
(
    ID                BIGINT PRIMARY KEY,
    CONTENT_ID        BIGINT        NOT NULL,
    MEDIA_LINK        VARCHAR(2048) NOT NULL,
    SOCIAL_MEDIA_SITE VARCHAR(16)   NOT NULL,
    ACTIVE            BOOLEAN       NOT NULL
);

ALTER TABLE PHP_TOKEN
    ADD FOREIGN KEY (USER_ID) REFERENCES PHP_USER (ID);

ALTER TABLE PHP_CONTENT
    ADD FOREIGN KEY (AUTHOR_ID) REFERENCES PHP_USER (ID);

ALTER TABLE PHP_EMAIL_ADDRESS
    ADD FOREIGN KEY (USER_ID) REFERENCES PHP_USER (ID);

CREATE SEQUENCE HIBERNATE_SEQUENCE;
