package tgr.phymosis.homepage.beans;


import org.springframework.lang.NonNull;

import java.util.Date;
import java.util.Objects;

/**
 * represents a class of security tokens, given to the administrators
 * to perform administrational operations.
 */
public class TokenBean
{
  @NonNull
  private final String token;

  @NonNull
  private final Date expirationTime;

  public TokenBean(@NonNull String token, @NonNull Date expirationTime)
  {
    this.token = token;
    this.expirationTime = expirationTime;
  }
  public TokenBean(@NonNull String tokenString)
  {
    String[] stringArray = tokenString.split(";");
    this.token = stringArray[0];
    this.expirationTime = new Date(Long.parseLong(stringArray[1]));
  }

  @NonNull
  public String getToken()
  {
    return token;
  }

  @NonNull
  public Date getExpirationTime()
  {
    return expirationTime;
  }

  @Override
  public boolean equals(Object o)
  {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;
    TokenBean tokenBean = (TokenBean) o;
    return getToken().equals(tokenBean.getToken()) &&
           getExpirationTime().equals(tokenBean.getExpirationTime());
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(getToken(), getExpirationTime());
  }
}
