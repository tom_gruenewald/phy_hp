package tgr.phymosis.homepage.beans;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Date;
import java.util.Objects;

public class TourdateBean
{

  @NonNull
  private final Long id;

  @Nullable
  private final String location;

  @NonNull
  private final Date date;

  @NonNull
  private final String title;

  @Nullable
  private final String googleMapsLink;

  @Nullable
  private final String text;

  @NonNull
  private final Boolean active;

  public TourdateBean(@NonNull Long id,
                      @Nullable String location,
                      @NonNull Date date,
                      @NonNull String title,
                      @Nullable String googleMapsLink,
                      @Nullable String text,
                      @NonNull Boolean active)
  {
    this.id = id;
    this.location = location;
    this.date = date;
    this.title = title;
    this.googleMapsLink = googleMapsLink;
    this.text = text;
    this.active = active;
  }

  @NonNull
  public Long getId()
  {
    return id;
  }

  @Nullable
  public String getLocation()
  {
    return location;
  }

  @NonNull
  public Date getDate()
  {
    return date;
  }

  @NonNull
  public String getTitle()
  {
    return title;
  }

  @Nullable
  public String getGoogleMapsLink()
  {
    return googleMapsLink;
  }

  @Nullable
  public String getText()
  {
    return text;
  }

  @NonNull
  public Boolean getActive()
  {
    return active;
  }

  @Override
  public boolean equals(Object o)
  {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;
    TourdateBean that = (TourdateBean) o;
    return getId().equals(that.getId()) &&
           Objects.equals(getLocation(), that.getLocation()) &&
           getDate().equals(that.getDate()) &&
           getTitle().equals(that.getTitle()) &&
           Objects.equals(getGoogleMapsLink(), that.getGoogleMapsLink()) &&
           Objects.equals(getText(), that.getText()) &&
           getActive().equals(that.getActive());
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(getId(), getLocation(), getDate(), getTitle(), getGoogleMapsLink(), getText(), getActive());
  }
}
