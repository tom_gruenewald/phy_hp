package tgr.phymosis.homepage.beans;

import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.beans.enums.SocialMediaSite;

import java.util.Objects;

public class SocialMediaLinkBean {
  @NonNull
  private Long id;
  @NonNull
  private Long contentId;
  @NonNull
  private String mediaLink;
  @NonNull
  private SocialMediaSite socialMediaSite;
  @NonNull
  private boolean active;

  public SocialMediaLinkBean(@NonNull Long id, @NonNull Long contentId, @NonNull String mediaLink, @NonNull SocialMediaSite socialMediaSite, boolean active)
  {
    this.id = id;
    this.contentId = contentId;
    this.mediaLink = mediaLink;
    this.socialMediaSite = socialMediaSite;
    this.active = active;
  }

  @NonNull
  public Long getId()
  {
    return id;
  }

  @NonNull
  public Long getContentId()
  {
    return contentId;
  }

  @NonNull
  public String getMediaLink()
  {
    return mediaLink;
  }

  @NonNull
  public SocialMediaSite getSocialMediaSite()
  {
    return socialMediaSite;
  }

  public boolean isActive()
  {
    return active;
  }

  @Override
  public boolean equals(Object o)
  {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;
    SocialMediaLinkBean that = (SocialMediaLinkBean) o;
    return isActive() == that.isActive() &&
           getId().equals(that.getId()) &&
           getContentId().equals(that.getContentId()) &&
           getMediaLink().equals(that.getMediaLink()) &&
           getSocialMediaSite() == that.getSocialMediaSite();
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(getId(), getContentId(), getMediaLink(), getSocialMediaSite(), isActive());
  }
}
