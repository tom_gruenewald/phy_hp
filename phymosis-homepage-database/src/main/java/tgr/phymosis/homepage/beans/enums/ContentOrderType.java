package tgr.phymosis.homepage.beans.enums;

import org.springframework.lang.NonNull;

public enum ContentOrderType
{
  DATE_DESC("DATE_DESC"),
  DATE_ASC("DATE_ASC"),
  TITLE_DESC("TITLE_DESC"),
  TITLE_ASC("TITLE_ASC");

  @NonNull
  private final String type;

  ContentOrderType(@NonNull String type)
  {
    this.type = type;
  }

  @NonNull
  public String getType()
  {
    return type;

  }

}
