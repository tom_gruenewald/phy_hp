package tgr.phymosis.homepage.beans.enums;

import org.springframework.lang.NonNull;

public enum SecurityLevel
{
  NONE("NONE"),
  ADMIN("ADMIN"),
  SUPER_ADMIN("SUPER_ADMIN"),
  BAND_MEMBER("BAND_MEMBER");

  @NonNull
  private final String type;

  SecurityLevel(@NonNull String type)
  {
    this.type = type;
  }

  @NonNull
  public String getType()
  {
    return type;
  }
}
