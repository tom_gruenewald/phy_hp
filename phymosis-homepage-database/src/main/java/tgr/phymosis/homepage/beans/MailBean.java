package tgr.phymosis.homepage.beans;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import java.util.Objects;

public class MailBean {

  @NonNull
  private String to;

  @NonNull
  private String from;

  @Nullable
  private String title;

  @Nullable
  private String text;

  public MailBean(@NonNull String to, @NonNull String from, @Nullable String title, @Nullable String text)
  {
    this.to = to;
    this.from = from;
    this.title = title;
    this.text = text;
  }

  @NonNull
  public String getTo()
  {
    return to;
  }

  @NonNull
  public String getFrom()
  {
    return from;
  }

  @Nullable
  public String getTitle()
  {
    return title;
  }

  @Nullable
  public String getText()
  {
    return text;
  }

  @Override
  public boolean equals(Object o)
  {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;
    MailBean mailBean = (MailBean) o;
    return getTo().equals(mailBean.getTo()) &&
           getFrom().equals(mailBean.getFrom()) &&
           Objects.equals(getTitle(), mailBean.getTitle()) &&
           Objects.equals(getText(), mailBean.getText());
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(getTo(), getFrom(), getTitle(), getText());
  }
}
