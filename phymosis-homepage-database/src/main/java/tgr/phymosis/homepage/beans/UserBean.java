package tgr.phymosis.homepage.beans;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.enums.SecurityLevel;

import java.util.Objects;

public class UserBean
{
  @NonNull
  private final Long id;

  @NonNull
  private final String name;

  @NonNull
  private final SecurityLevel securityLevel;

  @Nullable
  private final String emailAddress;

  @Nullable
  private TokenBean tokenBean;

  @NonNull
  private final Boolean active;

  public UserBean(@NonNull Long id,
                  @NonNull String name,
                  @NonNull SecurityLevel securityLevel,
                  @Nullable String emailAddress,
                  @Nullable TokenBean tokenBean,
                  @NonNull Boolean active)
  {
    this.id = id;
    this.name = name;
    this.securityLevel = securityLevel;
    this.emailAddress = emailAddress;
    this.tokenBean = tokenBean;
    this.active = active;
  }

  public UserBean(@NonNull Long id,
                  @NonNull String name,
                  @NonNull SecurityLevel securityLevel,
                  @Nullable String emailAddress,
                  @NonNull Boolean active)
  {
    this.id = id;
    this.name = name;
    this.securityLevel = securityLevel;
    this.emailAddress = emailAddress;
    this.tokenBean = null;
    this.active = active;
  }

  @NonNull
  public Long getId()
  {
    return id;
  }

  @NonNull
  public String getName()
  {
    return name;
  }

  @NonNull
  public SecurityLevel getSecurityLevel()
  {
    return securityLevel;
  }

  @Nullable
  public String getEmailAddress()
  {
    return emailAddress;
  }

  @NonNull
  public boolean getActive()
  {
    return active;
  }

  @Nullable
  public TokenBean getTokenBean()
  {
    return tokenBean;
  }

  public void setTokenBean(@Nullable TokenBean tokenBean)
  {
    this.tokenBean = tokenBean;
  }

  @Override
  public boolean equals(Object o)
  {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;
    UserBean userBean = (UserBean) o;
    return getId().equals(userBean.getId()) &&
           getName().equals(userBean.getName()) &&
           getSecurityLevel() == userBean.getSecurityLevel() &&
           Objects.equals(getEmailAddress(), userBean.getEmailAddress());
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(getId(), getName(), getSecurityLevel(), getEmailAddress());
  }
}
