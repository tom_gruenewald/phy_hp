package tgr.phymosis.homepage.beans.enums;

import org.springframework.lang.NonNull;

public enum SocialMediaSite
{
  YOUTUBE("YOUTUBE"),
  BANDCAMP("BANDCAMP"),
  FACEBOOK("FACEBOOK");

  @NonNull
  private final String type;

  SocialMediaSite(@NonNull String type)
  {
    this.type = type;
  }

  @NonNull
  public String getType()
  {
    return type;
  }
}

