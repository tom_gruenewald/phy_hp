package tgr.phymosis.homepage.beans.enums;

import org.springframework.lang.NonNull;

public enum MailErrorType {

  PARSE("PARSE"),
  AUTHENTICATION("AUTHENTICATION"),
  SEND("SEND");

  @NonNull
  private final String type;

  MailErrorType(@NonNull String type)
  {
    this.type = type;
  }

  @NonNull
  public String getType()
  {
    return type;
  }
}
