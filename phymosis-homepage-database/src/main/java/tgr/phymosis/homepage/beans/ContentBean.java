package tgr.phymosis.homepage.beans;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.enums.ContentType;
import tgr.phymosis.homepage.beans.enums.MediaType;

import java.util.Date;
import java.util.Objects;

public class ContentBean
{
  @NonNull
  private final Long id;

  @NonNull
  private final Long authorId;

  @NonNull
  private final ContentType contentType;

  @Nullable
  private final MediaType mediaType;

  @NonNull
  private final Date creationDate;

  @Nullable
  private final String title;

  @Nullable
  private final String text;

  @NonNull
  private final Boolean active;

  public ContentBean(@NonNull Long id, @NonNull Long authorId, @NonNull ContentType contentType, @Nullable MediaType mediaType,
                     @NonNull Date creationDate, @Nullable String title,
                     @Nullable String text, @NonNull Boolean active)
  {
    this.id = id;
    this.authorId = authorId;
    this.contentType = contentType;
    this.mediaType = mediaType;
    this.creationDate = creationDate;
    this.title = title;
    this.text = text;
    this.active = active;
  }

  @NonNull
  public Long getId()
  {
    return id;
  }

  @NonNull
  public Long getAuthorId()
  {
    return authorId;
  }

  @NonNull
  public ContentType getContentType()
  {
    return contentType;
  }

  @Nullable
  public MediaType getMediaType()
  {
    return mediaType;
  }

  @NonNull
  public Date getCreationDate()
  {
    return creationDate;
  }

  @Nullable
  public String getTitle()
  {
    return title;
  }

  @Nullable
  public String getText()
  {
    return text;
  }

  @Nullable
  public Boolean getActive()
  {
    return active;
  }

  @Override
  public boolean equals(Object o)
  {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;
    ContentBean that = (ContentBean) o;
    return getId().equals(that.getId()) &&
           getAuthorId().equals(that.getAuthorId()) &&
           getContentType() == that.getContentType() &&
           getMediaType() == that.getMediaType() &&
           getCreationDate().equals(that.getCreationDate()) &&
           Objects.equals(getTitle(), that.getTitle()) &&
           Objects.equals(getText(), that.getText()) &&
           Objects.equals(getActive(), that.getActive());
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(getId(), getAuthorId(), getContentType(), getMediaType(), getCreationDate(), getTitle(), getText(), getActive());
  }
}
