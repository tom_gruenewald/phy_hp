package tgr.phymosis.homepage.beans.enums;

import org.springframework.lang.NonNull;

public enum MediaType
{
  NONE("NONE"),
  YOUTUBE_VIDEO("YOUTUBE_VIDEO"),
  BANDCAMP_LINK("BANDCAMP_LINK"),
  IMAGE("IMAGE");

  @NonNull
  private final String type;

  MediaType(@NonNull String type)
  {
    this.type = type;
  }

  @NonNull
  public String getType()
  {
    return type;
  }
}
