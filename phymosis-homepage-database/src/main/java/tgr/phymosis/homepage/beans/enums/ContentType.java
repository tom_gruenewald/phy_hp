package tgr.phymosis.homepage.beans.enums;

import org.springframework.lang.NonNull;

public enum ContentType
{
  NEWS("NEWS"),
  OTHER("OTHER"),
  BAND_MEMBER("BAND_MEMBER"),
  RELEASE("RELEASE"),
  IMAGE("IMAGE"),
  SONG("SONG"),
  COMMENT("COMMENT");

  @NonNull
  private final String type;

  ContentType(@NonNull String type)
  {
    this.type = type;
  }

  @NonNull
  public String getType()
  {
    return type;
  }

}
