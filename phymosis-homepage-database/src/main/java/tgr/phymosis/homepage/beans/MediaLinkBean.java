package tgr.phymosis.homepage.beans;

import org.springframework.lang.NonNull;

import java.util.Objects;

public class MediaLinkBean
{
  @NonNull
  private final Long id;

  /**
   * contentId might be a UserId, eg. for an image,
   *  a BlogpostId or other Ids referring to media of the Homepage itself
   */
  @NonNull
  private final Long contentId;

  @NonNull
  private final String mediaLink;

  public MediaLinkBean(@NonNull Long id, @NonNull Long contentId, @NonNull String mediaLink)
  {
    this.id = id;
    this.contentId = contentId;
    this.mediaLink = mediaLink;
  }

  @NonNull
  public Long getId()
  {
    return id;
  }

  @NonNull
  public Long getContentId()
  {
    return contentId;
  }

  @NonNull
  public String getMediaLink()
  {
    return mediaLink;
  }

  @Override
  public boolean equals(Object o)
  {
    if(this == o) return true;
    if(o == null || getClass() != o.getClass()) return false;
    MediaLinkBean that = (MediaLinkBean) o;
    return getId().equals(that.getId()) &&
           getContentId().equals(that.getContentId()) &&
           getMediaLink().equals(that.getMediaLink());
  }

  @Override
  public int hashCode()
  {
    return Objects.hash(getId(), getContentId(), getMediaLink());
  }
}
