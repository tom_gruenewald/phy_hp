package tgr.phymosis.homepage.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.beans.enums.ContentType;
import tgr.phymosis.homepage.domain.ContentJPA;

public interface ContentRepository
  extends CrudRepository<ContentJPA, Long>
{


  int countByContentType(@Param("contentType") @NonNull ContentType contentType);

  int countByActiveAndContentType(@Param("active") @NonNull Boolean active,
                                  @Param("contentType") @NonNull ContentType contentType);

  @Query("SELECT content "
         + " FROM ContentJPA content "
         + " WHERE content.contentType = :contentType "
         + " AND content.active = true"
         + " ORDER BY content.title ASC ")
  Iterable<ContentJPA> findActiveByContentTypeOrderByTitleAscending(@Param("contentType") @NonNull ContentType contentType);

  @Query("SELECT content "
         + " FROM ContentJPA content "
         + " WHERE content.contentType = :contentType "
         + " ORDER BY content.title DESC ")
  Iterable<ContentJPA> findByContentTypeOrderByTitleDescending(@Param("contentType") @NonNull ContentType contentType);

  @Query("SELECT content "
         + " FROM ContentJPA content "
         + " WHERE content.contentType = :contentType "
         + " ORDER BY content.creationDate ASC ")
  Iterable<ContentJPA> findByContentTypeOrderByDateAscending(@Param("contentType") @NonNull ContentType contentType);

  @Query("SELECT content "
         + " FROM ContentJPA content "
         + " WHERE content.contentType = :contentType "
         + " ORDER BY content.creationDate DESC ")
  Iterable<ContentJPA> findByContentTypeOrderByDateDescending(@Param("contentType") @NonNull ContentType contentType);

  @Query("SELECT content "
         + " FROM ContentJPA content "
         + " WHERE content.contentType = :contentType "
         + " AND content.active = :active ")
  Page<ContentJPA> findByContentTypeAndActivePageable(@Param("contentType") @NonNull ContentType contentType,
                                                      @Param("active") @NonNull Boolean active,
                                                      Pageable pageable);

}
