package tgr.phymosis.homepage.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.beans.TourdateBean;
import tgr.phymosis.homepage.domain.TourdateJPA;

import java.util.Date;
import java.util.List;

public interface TourdateRepository
  extends CrudRepository<TourdateJPA, Long>
{
  @Query("SELECT tourdate"
         + " FROM TourdateJPA tourdate ")
  Page<TourdateJPA> findTourdatesByPage(Pageable pageable);

  @Query("SELECT tourdate"
         + " FROM TourdateJPA tourdate "
         + " WHERE tourdate.date >= :date")
  Page<TourdateJPA> findTourdatesPageAfterDate(@Param("date") @NonNull Date date,
                                               Pageable pageable);

  @Query("SELECT tourdate"
         + " FROM TourdateJPA tourdate "
         + " WHERE tourdate.date < :date")
  Page<TourdateJPA> findTourdatesPageBeforeDate(@Param("date") @NonNull Date date,
                                                Pageable pageable);

  @Query("SELECT tourdate"
         + " FROM TourdateJPA tourdate "
         + " WHERE tourdate.active = true")
  Iterable<TourdateJPA> findAllByActive();

  @Query("SELECT tourdate"
         + " FROM TourdateJPA tourdate "
         + " WHERE tourdate.active = true")
  Page<TourdateJPA> findActivePage(Pageable pageable);
}
