package tgr.phymosis.homepage.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.domain.SocialMediaLinkJPA;

import java.util.Optional;

public interface SocialMediaLinkRepository
  extends CrudRepository<SocialMediaLinkJPA, Long>
{

  int countByContentId(@NonNull Long contentId);


  @Query("SELECT socialMediaLink "
         + " FROM SocialMediaLinkJPA socialMediaLink "
         + " WHERE socialMediaLink.contentId = :contentId")
  Optional<SocialMediaLinkJPA> findByContentId(@Param("contentId") @NonNull Long contentId);

}
