package tgr.phymosis.homepage.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.domain.TokenJPA;
import tgr.phymosis.homepage.domain.TourdateJPA;

import java.util.Date;
import java.util.Optional;

public interface TokenRepository
  extends CrudRepository<TokenJPA, Long>
{
  @Query("SELECT token"
         + " FROM TokenJPA token "
         + " WHERE token.token = :token ")
  Optional<TokenJPA> findTokenByUserAndTokenString(@Param("token") @NonNull String token);

  @Modifying
  @Query("DELETE"
         + " FROM TokenJPA token "
         + " WHERE token.validated < :date")
  void deleteTokensBefore(@Param("date") @NonNull Date date);

  @Modifying
  @Query("DELETE"
         + " FROM TokenJPA token "
         + " WHERE token.userId = :userId")
  void deleteByUserId(@Param("userId") @NonNull Long userId);
}
