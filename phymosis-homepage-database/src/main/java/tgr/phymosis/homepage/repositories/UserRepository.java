package tgr.phymosis.homepage.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.domain.UserJPA;

import java.util.Optional;


public interface UserRepository
  extends CrudRepository<UserJPA, Long>
{
  int countByName(@NonNull String name);

  @Query("SELECT user"
         + " FROM UserJPA user "
         + " WHERE user.active = true "
         + " AND user.name = :name")
  Optional<UserJPA> findActiveByName(@Param("name") @NonNull String name);

  @Query("SELECT user"
         + " FROM UserJPA user "
         + " WHERE user.active = true "
         + " AND user.name = :name"
         + " AND user.password = :password")
  Optional<UserJPA> findActiveByNameAndPassword(@Param("name") @NonNull String name,
                                                @Param("password") @NonNull String password);

  @Query("SELECT user "
         + " FROM UserJPA user "
         + " ORDER BY user.id ASC")
  Iterable<UserJPA> findAllOrderByIdAsc();

  @Query("SELECT user"
         + " FROM UserJPA user "
         + " WHERE user.active = true "
         + " AND user.id = :userId ")
  Optional<UserJPA> findByIdAndActive(@Param("userId") @NonNull Long userId);

  @Query("SELECT user "
         + " FROM UserJPA user "
         + " WHERE user.active = true ")
  Page<UserJPA> findAllActiveInRange(Pageable pageable);

  @Query("SELECT user "
         + " FROM UserJPA user ")
  Page<UserJPA> findAllInRange(Pageable pageable);


}
