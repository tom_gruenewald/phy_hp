package tgr.phymosis.homepage.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.domain.EmailAddressJPA;

import java.util.Optional;

public interface EmailAddressRepository
  extends CrudRepository<EmailAddressJPA, Long>
{

  @Query("SELECT emailAddress"
         + " FROM EmailAddressJPA emailAddress"
         + " WHERE emailAddress.user.id = :userId")
  Optional<EmailAddressJPA> findByUserId(@Param("userId") @NonNull Long userId);
}
