package tgr.phymosis.homepage.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.domain.MediaLinkJPA;

public interface MediaLinkRepository
  extends CrudRepository<MediaLinkJPA, Long>
{
  @Query("SELECT mediaLink "
         + " FROM MediaLinkJPA mediaLink "
         + " WHERE mediaLink.contentId = :contentId")
  Iterable<MediaLinkJPA> findAllByContentId(@Param("contentId") @NonNull Long contentId);

}
