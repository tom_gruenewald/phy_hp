package tgr.phymosis.homepage.util;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.ContentBean;
import tgr.phymosis.homepage.beans.TourdateBean;
import tgr.phymosis.homepage.domain.ContentJPA;
import tgr.phymosis.homepage.domain.TourdateJPA;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class TourdateBeanUtil {

  @Nullable
  public static TourdateBean convert(@NonNull TourdateJPA tourdateJPA)
  {

    return tourdateJPA.getId() != null
           ? new  TourdateBean(tourdateJPA.getId(),
                               tourdateJPA.getLocation(),
                               tourdateJPA.getDate(),
                               tourdateJPA.getTitle(),
                               tourdateJPA.getGoogleMapsLink(),
                               tourdateJPA.getText(),
                               tourdateJPA.getActive())
           : null;
  }

  @NonNull
  public static List<TourdateBean> convertAll(@Nullable Iterable<TourdateJPA> tourdateJPAIterable)
  {
    return tourdateJPAIterable != null
           ? StreamSupport.stream(tourdateJPAIterable.spliterator(), false)
             .map(TourdateBeanUtil::convert)
             .collect(Collectors.toList())
           : new ArrayList<>();
  }

}
