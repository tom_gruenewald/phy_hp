package tgr.phymosis.homepage.util;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.UserBean;
import tgr.phymosis.homepage.domain.UserJPA;

public class UserBeanUtil {

  @NonNull
  public static UserBean convert(@NonNull UserJPA userJPA, @Nullable String emailAddress)
  {
    return new UserBean(userJPA.getId(),
                        userJPA.getName(),
                        userJPA.getSecurityLevel(),
                        emailAddress,
                        userJPA.isActive());
  }
}
