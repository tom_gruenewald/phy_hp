package tgr.phymosis.homepage.util;

import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.beans.TokenBean;
import tgr.phymosis.homepage.domain.TokenJPA;
import tgr.phymosis.homepage.domain.UserJPA;

import javax.security.auth.login.CredentialException;

import java.util.Date;
import java.util.Random;

public class TokenBeanUtil {
  @NonNull
  public static TokenBean convert(@NonNull TokenJPA tokenJPA)
  {
    return new TokenBean(tokenJPA.getToken(),
                         tokenJPA.getValidated());
  }

  @NonNull
  public static String generateTokenKey(@NonNull UserJPA userJPA)
  {
    int leftLimit = 49;
    int rightLimit = 121;
    int targetStringLength = 100;
    Random random = new Random();

    String generatedToken = random.ints(leftLimit, rightLimit + 1)
      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
      .limit(targetStringLength)
      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
      .toString();

    int idLength = Long.toString(userJPA.getId()).length();
    int insertionPosition = random.nextInt(100 - (Long.toString(userJPA.getId()).length() + 2));
    String replacedSubString = generatedToken.substring(insertionPosition, insertionPosition + idLength);
    generatedToken += Long.toString((new Date()).getTime());
    return generatedToken.replace(replacedSubString, "z" + userJPA.getId() + "0");
  }
}
