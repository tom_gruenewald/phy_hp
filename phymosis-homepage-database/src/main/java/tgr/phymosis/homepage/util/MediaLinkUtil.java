package tgr.phymosis.homepage.util;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.MediaLinkBean;
import tgr.phymosis.homepage.domain.MediaLinkJPA;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MediaLinkUtil
{

  @Nullable
  public static MediaLinkBean convert(@NonNull MediaLinkJPA mediaLinkJPA)
  {
    return mediaLinkJPA.getId() != null
           ? new MediaLinkBean(mediaLinkJPA.getId(),
                               mediaLinkJPA.getContentId(),
                               mediaLinkJPA.getMediaLink())
           : null;
  }

  @NonNull
  public static List<MediaLinkBean> convertAll(@NonNull Iterable<MediaLinkJPA> mediaLinkJPAIterable)
  {
    return StreamSupport.stream(mediaLinkJPAIterable.spliterator(), false)
      .map(MediaLinkUtil::convert)
      .collect(Collectors.toList());
  }
}
