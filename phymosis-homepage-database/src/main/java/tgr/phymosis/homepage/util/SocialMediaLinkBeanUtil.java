package tgr.phymosis.homepage.util;

import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.beans.SocialMediaLinkBean;
import tgr.phymosis.homepage.domain.SocialMediaLinkJPA;

public class SocialMediaLinkBeanUtil {
  @NonNull
  public static SocialMediaLinkBean convert(@NonNull SocialMediaLinkJPA socialMediaLinkJPA)
  {
    return new SocialMediaLinkBean(socialMediaLinkJPA.getId(),
                                   socialMediaLinkJPA.getContentId(),
                                   socialMediaLinkJPA.getMediaLink(),
                                   socialMediaLinkJPA.getSocialMediaSite(),
                                   socialMediaLinkJPA.getActive());
  }
}
