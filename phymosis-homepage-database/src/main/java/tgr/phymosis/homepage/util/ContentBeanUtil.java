package tgr.phymosis.homepage.util;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.ContentBean;
import tgr.phymosis.homepage.domain.ContentJPA;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ContentBeanUtil
{

  @Nullable
  public static ContentBean convert(@NonNull ContentJPA contentJPA)
  {

    return contentJPA.getId() != null && contentJPA.getActive() != null
           ? new ContentBean(contentJPA.getId(),
                             contentJPA.getAuthor().getId(),
                             contentJPA.getContentType(),
                             contentJPA.getMediaType(),
                             contentJPA.getCreationDate(),
                             contentJPA.getTitle(),
                             contentJPA.getText(),
                             contentJPA.getActive())
           : null;
  }

  @NonNull
  public static List<ContentBean> convertAll(@Nullable Iterable<ContentJPA> contentJPAIterable)
  {
    return contentJPAIterable != null
           ? StreamSupport.stream(contentJPAIterable.spliterator(), false)
             .map(ContentBeanUtil::convert)
             .collect(Collectors.toList())
           : new ArrayList<>();
  }

}
