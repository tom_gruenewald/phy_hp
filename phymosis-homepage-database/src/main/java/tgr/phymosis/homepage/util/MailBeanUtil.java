package tgr.phymosis.homepage.util;

import org.springframework.lang.NonNull;
import org.springframework.mail.SimpleMailMessage;
import tgr.phymosis.homepage.beans.MailBean;

import java.util.Objects;

public class MailBeanUtil {

  @NonNull
  public static MailBean convert(@NonNull SimpleMailMessage simpleMailMessage)
  {
    String from = simpleMailMessage.getFrom() != null
                  ? simpleMailMessage.getFrom()
                  : "error";
    String to = Objects.requireNonNull(simpleMailMessage.getTo())[0] != null
                  ? simpleMailMessage.getTo()[0]
                  : "error";
    return new MailBean(from, to, simpleMailMessage.getSubject(), simpleMailMessage.getText());
  }
}
