package tgr.phymosis.homepage.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import tgr.phymosis.homepage.beans.ContentBean;
import tgr.phymosis.homepage.beans.enums.ContentOrderType;
import tgr.phymosis.homepage.beans.enums.ContentType;
import tgr.phymosis.homepage.beans.enums.MediaType;
import tgr.phymosis.homepage.domain.ContentJPA;
import tgr.phymosis.homepage.domain.UserJPA;
import tgr.phymosis.homepage.exceptions.ConversionErrorException;
import tgr.phymosis.homepage.exceptions.CouldNotCreateEntryException;
import tgr.phymosis.homepage.exceptions.EntryNotFoundException;
import tgr.phymosis.homepage.repositories.ContentRepository;
import tgr.phymosis.homepage.repositories.UserRepository;
import tgr.phymosis.homepage.util.ContentBeanUtil;

import java.util.Date;
import java.util.List;

@Component
public class ContentWorker
{

  private static final Logger LOGGER = LoggerFactory.getLogger(ContentWorker.class);

  private final ContentRepository contentRepository;
  private final UserRepository userRepository;

  public ContentWorker(ContentRepository contentRepository,
                       UserRepository userRepository)
  {
    this.contentRepository = contentRepository;
    this.userRepository = userRepository;
  }

  public ContentBean createContent(@NonNull Long authorId,
                                   @NonNull ContentType contentType,
                                   @Nullable MediaType mediaType,
                                   @NonNull Date creationDate,
                                   @Nullable String title,
                                   @Nullable String text)
  {
    try
    {
      UserJPA author = this.userRepository.findById(authorId)
        .orElseThrow(() -> new CouldNotCreateEntryException("Requested author with id: " + authorId + " not found."));
      ContentBean contentBean = ContentBeanUtil
        .convert(this.contentRepository
                   .save(new ContentJPA(author,
                                        contentType,
                                        mediaType,
                                        creationDate,
                                        title,
                                        text)
                   )
        );
      if(contentBean == null)
      {
        throw new ConversionErrorException("Could not convert Content JPA with creation date: " + creationDate);
      }
      return contentBean;
    }
    catch(RuntimeException ex)
    {
      return null;
    }
  }

  public ContentBean getContentById(@NonNull Long contentId)
  {
    try
    {
      return ContentBeanUtil.convert(this.contentRepository.findById(contentId)
                                       .orElseThrow(() -> new EntryNotFoundException("Content with ID: " + contentId + " coud not be found.")));
    }
    catch(EntryNotFoundException ex)
    {
      return null;
    }
  }

  public List<ContentBean> getAllContents()
  {
    return ContentBeanUtil.convertAll(this.contentRepository.findAll());
  }

  public List<ContentBean> getActiveContentByType(@NonNull ContentType contentType,
                                                  @NonNull ContentOrderType contentOrderType)
  {
    if(contentOrderType == ContentOrderType.DATE_DESC)
    {
      return ContentBeanUtil
        .convertAll(this.contentRepository.findByContentTypeOrderByDateDescending(contentType));
    }
    if(contentOrderType == ContentOrderType.TITLE_ASC)
    {
      return ContentBeanUtil
        .convertAll(this.contentRepository.findActiveByContentTypeOrderByTitleAscending(contentType));
    }
    if(contentOrderType == ContentOrderType.TITLE_DESC)
    {
      return ContentBeanUtil
        .convertAll(this.contentRepository.findByContentTypeOrderByTitleDescending(contentType));
    }
    return ContentBeanUtil
      .convertAll(this.contentRepository.findByContentTypeOrderByDateAscending(contentType));
  }

  public List<ContentBean> getContentPageableByType(@NonNull ContentType contentType,
                                                    @NonNull Integer page,
                                                    @NonNull Integer size,
                                                    @NonNull ContentOrderType contentOrderType,
                                                    @NonNull Boolean active)
  {
    if(contentOrderType == ContentOrderType.DATE_DESC)
    {
      return ContentBeanUtil
        .convertAll(
          this.contentRepository
            .findByContentTypeAndActivePageable(contentType, active, PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "creationDate")))
        );
    }
    if(contentOrderType == ContentOrderType.TITLE_ASC)
    {
      return ContentBeanUtil
        .convertAll(
          this.contentRepository
            .findByContentTypeAndActivePageable(contentType, active, PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "title")))
        );
    }
    if(contentOrderType == ContentOrderType.TITLE_DESC)
    {
      return ContentBeanUtil
        .convertAll(
          this.contentRepository
            .findByContentTypeAndActivePageable(contentType, active, PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "title")))
        );
    }
    return ContentBeanUtil
      .convertAll(
        this.contentRepository
          .findByContentTypeAndActivePageable(contentType, active, PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "creationDate")))
      );
  }


  public boolean deactivateContentById(@NonNull Long contentId)
  {
    ContentJPA contentJPA = this.contentRepository.findById(contentId)
      .orElseThrow(() -> new EntryNotFoundException("Content with ID: " + contentId + " could not be found."));
    contentJPA.setActive(false);
    this.contentRepository.save(contentJPA);
    Boolean value = this.contentRepository.findById(contentId)
             .orElseThrow(() -> new EntryNotFoundException("Content with ID: " + contentId + " could not be found."))
             .getActive();
    return value != null && !value;
  }


  public ContentBean reactivateContentById(@NonNull Long contentId)
  {
    ContentJPA contentJPA = this.contentRepository.findById(contentId)
      .orElseThrow(() -> new EntryNotFoundException("Content with ID: " + contentId + " could not be found."));
    contentJPA.setActive(true);
    this.contentRepository.save(contentJPA);
    contentJPA = this.contentRepository.findById(contentId)
             .orElseThrow(() -> new EntryNotFoundException("Content with ID: " + contentId + " could not be found."));
    return contentJPA.getActive() != null && contentJPA.getActive()
           ? ContentBeanUtil.convert(contentJPA)
           : null;
  }

  public int getNumberOfActiveContentByType(ContentType contentType)
  {
    return this.contentRepository.countByActiveAndContentType(true, contentType);
  }

  public int getNumberOfContentByType(ContentType contentType)
  {
    return this.contentRepository.countByContentType(contentType);
  }
}
