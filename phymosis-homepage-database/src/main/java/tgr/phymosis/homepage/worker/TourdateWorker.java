package tgr.phymosis.homepage.worker;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import tgr.phymosis.homepage.beans.TourdateBean;
import tgr.phymosis.homepage.domain.TourdateJPA;
import tgr.phymosis.homepage.exceptions.EntryNotFoundException;
import tgr.phymosis.homepage.repositories.TourdateRepository;
import tgr.phymosis.homepage.util.TourdateBeanUtil;

import java.util.Date;
import java.util.List;

@Component
public class TourdateWorker
{

  private final TourdateRepository tourdateRepository;

  public TourdateWorker(TourdateRepository tourdateRepository)
  {
    this.tourdateRepository = tourdateRepository;
  }

  public TourdateBean createTourdate(@Nullable String location,
                                     @NonNull Date date,
                                     @NonNull String title,
                                     @Nullable String googleMapsLink,
                                     @Nullable String text)
  {
    return TourdateBeanUtil
      .convert(this.tourdateRepository
                 .save(new TourdateJPA(location,
                                       date,
                                       title,
                                       googleMapsLink,
                                       text)));
  }

  public TourdateBean getTourdateById(Long tourdateId)
  {
    TourdateJPA tourdateJPA = this.tourdateRepository
      .findById(tourdateId)
      .orElseThrow(() -> new EntryNotFoundException("Tourdate with id: " + tourdateId + " could not be found."));
    return TourdateBeanUtil.convert(tourdateJPA);
  }

  public List<TourdateBean> getAllTourdates()
  {
    return TourdateBeanUtil.convertAll(this.tourdateRepository.findAll());
  }

  public List<TourdateBean> getTourdatesPage(Integer page,
                                             Integer size)
  {
    return TourdateBeanUtil
      .convertAll(this.tourdateRepository
                    .findTourdatesByPage(PageRequest.of(page,
                                                        size,
                                                        Sort.by(Sort.Direction.ASC, "date")))
      );
  }

  public boolean deactivateTourdateById(Long tourdateId)
  {
    try
    {
      TourdateJPA tourdateJPA = this.tourdateRepository.findById(tourdateId)
        .orElseThrow(() -> new EntryNotFoundException("Tourdate with id: " + tourdateId + " could not be found."));
      tourdateJPA.setActive(false);
      this.tourdateRepository.save(tourdateJPA);
      return !this.tourdateRepository.findById(tourdateId)
        .orElseThrow(() -> new EntryNotFoundException("Tourdate with id: " + tourdateId + " could not be found."))
        .getActive();
    }
    catch(RuntimeException ex)
    {
      return false;
    }
  }

  public TourdateBean reactivateTourdateById(Long tourdateId)
  {
    try
    {
      TourdateJPA tourdateJPA = this.tourdateRepository.findById(tourdateId)
        .orElseThrow(() -> new EntryNotFoundException("Tourdate with id: " + tourdateId + " could not be found."));
      tourdateJPA.setActive(true);
      this.tourdateRepository.save(tourdateJPA);
      tourdateJPA = this.tourdateRepository.findById(tourdateId)
        .orElseThrow(() -> new EntryNotFoundException("Tourdate with id: " + tourdateId + " could not be found."));
      return tourdateJPA.getActive()
             ? TourdateBeanUtil.convert(tourdateJPA)
             : null;
    }
    catch(RuntimeException ex)
    {
      return null;
    }
  }

  public List<TourdateBean> getAllActiveTourdates()
  {
    return TourdateBeanUtil
      .convertAll(this.tourdateRepository.findAllByActive());
  }

  public List<TourdateBean> getActiveTourdatesPage(Integer page, Integer size)
  {
    return TourdateBeanUtil
      .convertAll(this.tourdateRepository.findActivePage(PageRequest.of(page,
                                                                        size,
                                                                        Sort.by(Sort.Direction.ASC, "date"))));
  }

  public List<TourdateBean> getPastTourdatesPage(Integer page, Integer size)
  {
    return TourdateBeanUtil.convertAll(this.tourdateRepository.findTourdatesPageBeforeDate(new Date(),
                                                                                           PageRequest.of(page,
                                                                                                            size,
                                                                                                            Sort.by(Sort.Direction.DESC, "date")
                                                                                             )
                                       )
    );
  }

  public List<TourdateBean> getUpcomingTourdatesPage(Integer page, Integer size)
  {
    return TourdateBeanUtil.convertAll(this.tourdateRepository.findTourdatesPageAfterDate(new Date(),
                                                                                             PageRequest.of(page,
                                                                                                            size,
                                                                                                            Sort.by(Sort.Direction.ASC, "date")
                                                                                             )
                                       )
    );
  }
}
