package tgr.phymosis.homepage.worker;

import org.springframework.stereotype.Component;
import tgr.phymosis.homepage.beans.MediaLinkBean;
import tgr.phymosis.homepage.domain.MediaLinkJPA;
import tgr.phymosis.homepage.exceptions.EntryNotFoundException;
import tgr.phymosis.homepage.repositories.MediaLinkRepository;
import tgr.phymosis.homepage.util.MediaLinkUtil;

import java.util.List;

@Component
public class MediaLinkWorker
{
  private final MediaLinkRepository mediaLinkRepository;

  public MediaLinkWorker(MediaLinkRepository mediaLinkRepository)
  {
    this.mediaLinkRepository = mediaLinkRepository;
  }

  public MediaLinkBean createMediaLink(Long contentId, String mediaLink)
  {
    return MediaLinkUtil.convert(this.mediaLinkRepository.save(new MediaLinkJPA(contentId, mediaLink)));
  }

  public MediaLinkBean getMediaLinkById(Long mediaLinkId)
  {
    return MediaLinkUtil
      .convert(this.mediaLinkRepository.findById(mediaLinkId)
                 .orElseThrow(() -> new EntryNotFoundException("MediaLink with ID: " + mediaLinkId + " coud not be found.")));
  }

  public List<MediaLinkBean> getAllMediaLinks()
  {
    return MediaLinkUtil.convertAll(this.mediaLinkRepository.findAll());
  }

  public List<MediaLinkBean> getMediaLinksByContentId(Long contentId)
  {
    return MediaLinkUtil.convertAll(this.mediaLinkRepository.findAllByContentId(contentId));
  }

  public MediaLinkBean deleteMediaLinkById(Long mediaLinkId)
  {
    MediaLinkBean mediaLinkBean = MediaLinkUtil
      .convert(this.mediaLinkRepository.findById(mediaLinkId)
                 .orElseThrow(() -> new EntryNotFoundException("MediaLink with ID: " + mediaLinkId + " coud not be found.")));
    this.mediaLinkRepository.deleteById(mediaLinkId);
    return mediaLinkBean;
  }

  public List<MediaLinkBean> deleteMediaLinksByContentId(Long contentId)
  {
    List<MediaLinkBean> mediaLinkBeans = MediaLinkUtil
      .convertAll(this.mediaLinkRepository.findAllByContentId(contentId));
    for(MediaLinkBean mediaLinkBean : mediaLinkBeans)
    {
      this.mediaLinkRepository.deleteById(mediaLinkBean.getId());
    }
    return mediaLinkBeans;
  }
}
