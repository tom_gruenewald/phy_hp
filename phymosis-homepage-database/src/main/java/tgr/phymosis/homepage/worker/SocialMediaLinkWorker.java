package tgr.phymosis.homepage.worker;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import tgr.phymosis.homepage.beans.SocialMediaLinkBean;
import tgr.phymosis.homepage.beans.enums.SocialMediaSite;
import tgr.phymosis.homepage.domain.SocialMediaLinkJPA;
import tgr.phymosis.homepage.exceptions.CouldNotCreateEntryException;
import tgr.phymosis.homepage.exceptions.EntryNotFoundException;
import tgr.phymosis.homepage.repositories.SocialMediaLinkRepository;
import tgr.phymosis.homepage.util.SocialMediaLinkBeanUtil;

@Component
public class SocialMediaLinkWorker
{
  private final SocialMediaLinkRepository socialMediaLinkRepository;

  public SocialMediaLinkWorker(SocialMediaLinkRepository socialMediaLinkRepository)
  {
    this.socialMediaLinkRepository = socialMediaLinkRepository;
  }

  public SocialMediaLinkBean getByContentId(@NonNull Long contentId)
  {
    return SocialMediaLinkBeanUtil
      .convert(this.socialMediaLinkRepository
                 .findByContentId(contentId)
                 .orElseThrow(() -> new EntryNotFoundException("SocialMediaLink",
                                                               "contentId",
                                                               contentId.toString())));
  }

  public SocialMediaLinkBean createSocialMediaLink(@NonNull Long contentId,
                                                   @NonNull String mediaLink,
                                                   @NonNull SocialMediaSite socialMediaSite)
  {
    if(socialMediaLinkRepository.countByContentId(contentId) == 0)
    {
      return SocialMediaLinkBeanUtil
        .convert(this.socialMediaLinkRepository
                   .save(new SocialMediaLinkJPA(contentId,
                                                mediaLink,
                                                socialMediaSite)));
    }
    throw new CouldNotCreateEntryException("A user with contentId: " + contentId + "already exists.");
  }

  public SocialMediaLinkBean changeSocialMediaLink(Long contentId, String mediaLink, SocialMediaSite socialMediaSite)
  {
    if(socialMediaLinkRepository.countByContentId(contentId) != 0)
    {
      Long linkId = this.socialMediaLinkRepository
        .findByContentId(contentId)
        .orElseThrow(() -> new EntryNotFoundException("SocialMediaLink",
                                                      "contentId",
                                                      contentId.toString())).getId();
      this.socialMediaLinkRepository.deleteById(linkId);
    }
    return this.createSocialMediaLink(contentId,mediaLink,socialMediaSite);
  }

  public SocialMediaLinkBean deactivateSocialMediaLinkByContentId(Long contentId)
  {
    if(socialMediaLinkRepository.countByContentId(contentId) == 1)
    {
      SocialMediaLinkJPA socialMediaLinkJPA = this.socialMediaLinkRepository
        .findByContentId(contentId)
        .orElseThrow(() -> new EntryNotFoundException("SocialMediaLink",
                                                      "contentId",
                                                      contentId.toString()));
      socialMediaLinkJPA.setActive(!socialMediaLinkJPA.getActive());
      this.socialMediaLinkRepository.save(socialMediaLinkJPA);
    }
    throw new EntryNotFoundException("SocialMediaLink",
                                     "contentId",
                                     contentId.toString());
  }
}
