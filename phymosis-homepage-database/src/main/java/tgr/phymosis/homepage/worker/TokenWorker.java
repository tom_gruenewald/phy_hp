package tgr.phymosis.homepage.worker;


import org.springframework.stereotype.Component;
import tgr.phymosis.homepage.beans.TokenBean;
import tgr.phymosis.homepage.domain.TokenJPA;
import tgr.phymosis.homepage.domain.UserJPA;
import tgr.phymosis.homepage.exceptions.EntryNotFoundException;
import tgr.phymosis.homepage.repositories.TokenRepository;
import tgr.phymosis.homepage.repositories.UserRepository;
import tgr.phymosis.homepage.util.TokenBeanUtil;

import java.util.Date;

@Component
public class TokenWorker
{

  private static final Long EXPIRATION_TIME = 3600000L;

  private final UserRepository userRepository;

  private final TokenRepository tokenRepository;

  public TokenWorker(UserRepository userRepository, TokenRepository tokenRepository)
  {
    this.userRepository = userRepository;
    this.tokenRepository = tokenRepository;
  }

  public TokenBean createToken(String userName, String password)
  {
    UserJPA userJPA = this.userRepository.findActiveByName(userName).orElseThrow(() -> new EntryNotFoundException("User not found."));
    if(userJPA.getPassword() != null && userJPA.getPassword().equals(password))
    {
      TokenJPA tokenJPA = this.tokenRepository.save(new TokenJPA(TokenBeanUtil.generateTokenKey(userJPA),
                                                                 userJPA.getId(),
                                                                 new Date()));
      return TokenBeanUtil.convert(tokenJPA);
    }
    return null;
  }

  public Boolean checkToken(TokenBean tokenBean)
  {
    try
    {
      Date date = new Date();
      TokenJPA tokenJPA = this.tokenRepository.findTokenByUserAndTokenString(tokenBean.getToken())
        .orElseThrow(() -> new EntryNotFoundException("User not found."));
      if(tokenJPA.getId() != null
         && tokenBean.getToken().contains("z" + tokenJPA.getUserId() + "0")
         && tokenJPA.getValidated().getTime() + EXPIRATION_TIME > date.getTime())
      {
        tokenJPA.setValidated(new Date());
        tokenRepository.save(tokenJPA);
        return true;
      }
      return false;
    }
    catch(EntryNotFoundException ex)
    {
      return false;
    }
  }

  public void deleteExpiredTokens()
  {
    this.tokenRepository.deleteTokensBefore(new Date(new Date().getTime() - EXPIRATION_TIME));
  }

  public void deleteTokensByUserId(Long userId)
  {
    this.tokenRepository.deleteByUserId(userId);
  }
}
