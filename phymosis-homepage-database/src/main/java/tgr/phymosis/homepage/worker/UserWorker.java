package tgr.phymosis.homepage.worker;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import tgr.phymosis.homepage.beans.UserBean;
import tgr.phymosis.homepage.domain.EmailAddressJPA;
import tgr.phymosis.homepage.domain.UserJPA;
import tgr.phymosis.homepage.exceptions.CouldNotCreateEntryException;
import tgr.phymosis.homepage.exceptions.EntryNotFoundException;
import tgr.phymosis.homepage.repositories.EmailAddressRepository;
import tgr.phymosis.homepage.repositories.UserRepository;
import tgr.phymosis.homepage.util.UserBeanUtil;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Component
public class UserWorker
{
  private static final Logger LOGGER = LoggerFactory.getLogger(UserWorker.class);

  private final UserRepository userRepository;
  private final EmailAddressRepository emailAddressRepository;

  public UserWorker(UserRepository userRepository,
                    EmailAddressRepository emailAddressRepository)
  {
    this.userRepository = userRepository;
    this.emailAddressRepository = emailAddressRepository;
  }


  public UserBean getUserById(Long userId)
  {
    UserJPA userJPA = this.userRepository
      .findById(userId)
      .orElseThrow(() -> new EntryNotFoundException("user",
                                                    "id",
                                                    userId.toString()));
    EmailAddressJPA emailAddressJPA = this.emailAddressRepository
      .findByUserId(userId).orElse(null);

    if(emailAddressJPA != null)
    {
      return UserBeanUtil.convert(userJPA, emailAddressJPA.getEmailAddress());
    }
    return UserBeanUtil.convert(userJPA, null);
  }

  public UserBean getActiveUserById(@NonNull Long userId)
  {
    UserJPA userJPA = this.userRepository
      .findByIdAndActive(userId)
      .orElseThrow(() -> new EntryNotFoundException("user",
                                                    "id",
                                                    userId.toString()));
    EmailAddressJPA emailAddressJPA = this.emailAddressRepository
      .findByUserId(userId).orElse(null);

    if(emailAddressJPA != null)
    {
      return UserBeanUtil.convert(userJPA, emailAddressJPA.getEmailAddress());
    }
    return UserBeanUtil.convert(userJPA, null);
  }

  public UserBean create(@NonNull String userName)
  {
    // If user does not already exist
    if(userRepository.countByName(userName) == 0)
    {
      UserJPA userJPA = this.userRepository.save(new UserJPA(userName));
      return getActiveUserById(userJPA.getId());
    }
    throw new CouldNotCreateEntryException("A user with name: " + userName + "already exists.");
  }

  public List<UserBean> getAllUsers()
  {
    Iterable<UserJPA> userJPAIterable = this.userRepository.findAllOrderByIdAsc();
    if(userJPAIterable != null)
    {
      List<UserBean> userBeanList = StreamSupport.stream(userJPAIterable.spliterator(), false)
        .map(
          userJPA -> this.getActiveUserById(userJPA.getId())
        )
        .collect(Collectors.toList());
      if(!userBeanList.isEmpty())
      {
        return userBeanList;
      }
    }
    return null;
  }

  public boolean deactivateUserById(@NonNull Long userId)
  {
    try
    {
      UserJPA userJPA = this.userRepository.findById(userId)
        .orElseThrow(() -> new EntryNotFoundException("user", "id", userId.toString()));
      userJPA.setActive(false);
      this.userRepository.save(userJPA);
      return !this.userRepository.findById(userId)
        .orElseThrow(() -> new EntryNotFoundException("user", "id", userId.toString())).isActive();
    }
    catch(RuntimeException ex)
    {
      LOGGER.warn("Could not deactivate User with id:" + userId, ex);
      return false;
    }
  }

  public List<UserBean> getUsersByPage(@NonNull Integer page,
                                       @NonNull Integer size,
                                       @NonNull Boolean getDeactivatedUsers)
  {
    Page<UserJPA> userJPAs;
    if(getDeactivatedUsers)
    {
      userJPAs = this.userRepository.findAllInRange(PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "name")));
    }
    else
    {
      userJPAs = this.userRepository.findAllActiveInRange(PageRequest.of(page, size, Sort.by(Sort.Direction.ASC, "name")));
    }
    return StreamSupport.stream(userJPAs.spliterator(), false)
      .map(userJPA -> {
        EmailAddressJPA emailAddressJPA = this.emailAddressRepository.findByUserId(userJPA.getId()).orElse(null);
        String email = emailAddressJPA != null
                       ? emailAddressJPA.getEmailAddress()
                       : null;
        return UserBeanUtil.convert(userJPA, email);
      }).collect(Collectors.toList());
  }

  public boolean setUserPassword(@NonNull Long userId,
                                 @NonNull String newPassword,
                                 @Nullable String oldPassword)
  {
    UserJPA userJPA = this.userRepository.findById(userId)
      .orElseThrow(() -> new EntryNotFoundException("user", "id", userId.toString()));
    if(userJPA.getPassword() == null && oldPassword == null)
    {
      userJPA.setPassword(newPassword);
      this.userRepository.save(userJPA);
    }
    if(userJPA.getPassword() != null && userJPA.getPassword().equals(oldPassword))
    {
      userJPA.setPassword(newPassword);
      this.userRepository.save(userJPA);
    }
    UserJPA userJPATest = this.userRepository.findById(userId)
      .orElseThrow(() -> new EntryNotFoundException("user", "id", userId.toString()));

    return userJPATest.getPassword() != null
           && userJPATest.getPassword().equals(newPassword);
  }

  public UserBean getUserByNameAndPassword(String name, String password)
  {
    UserJPA userJPA = this.userRepository.findActiveByNameAndPassword(name, password)
      .orElseThrow(() -> new EntryNotFoundException("user",
                                                    "name",
                                                    name));

    EmailAddressJPA emailAddressJPA = this.emailAddressRepository
      .findByUserId(userJPA.getId()).orElse(null);

    if(emailAddressJPA != null)
    {
      return UserBeanUtil.convert(userJPA, emailAddressJPA.getEmailAddress());
    }
    return UserBeanUtil.convert(userJPA, null);
  }
}
