package tgr.phymosis.homepage.exceptions;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

public class EntryNotFoundException
  extends RuntimeException
{
  public EntryNotFoundException(@NonNull String type, @NonNull String column, @Nullable String wrongValue)
  {
    super(String.format("The %s with \"%s\": \"%s\" could not be found.", type, column.toUpperCase(), wrongValue));
  }

  public EntryNotFoundException(@NonNull String reason)
  {
    super(reason);
  }
}
