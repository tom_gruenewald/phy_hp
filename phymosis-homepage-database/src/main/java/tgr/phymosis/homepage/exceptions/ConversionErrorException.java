package tgr.phymosis.homepage.exceptions;

import org.springframework.lang.NonNull;

public class ConversionErrorException
  extends RuntimeException
{
  public ConversionErrorException(@NonNull String reason)
  {
    super(reason);
  }
}
