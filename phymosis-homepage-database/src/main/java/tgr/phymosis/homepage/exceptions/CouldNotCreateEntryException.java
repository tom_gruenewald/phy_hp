package tgr.phymosis.homepage.exceptions;

import org.springframework.lang.NonNull;

public class CouldNotCreateEntryException
  extends RuntimeException
{
  public CouldNotCreateEntryException(@NonNull String reason)
  {
    super(reason);
  }
}
