package tgr.phymosis.homepage.watchdogs;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import tgr.phymosis.homepage.service.TokenService;

@Service
public class UserTokenWatchdog {
  private final TokenService tokenService;

  public UserTokenWatchdog(TokenService tokenService)
  {
    this.tokenService = tokenService;
  }

  @Scheduled(fixedRate = 600000)
  public void cleanTokens()
  {
    this.tokenService.deleteExpiredTokens();
  }
}
