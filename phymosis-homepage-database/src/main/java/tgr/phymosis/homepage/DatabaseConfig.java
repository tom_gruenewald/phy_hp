/*
 * Copyright (c) 2019 conLeos GmbH. All Rights reserved.
 *
 * This software is the confidential intellectual property of conLeos GmbH; it is copyrighted and licensed.
 */

package tgr.phymosis.homepage;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = DatabaseConfig.class)
public class DatabaseConfig
{
}
