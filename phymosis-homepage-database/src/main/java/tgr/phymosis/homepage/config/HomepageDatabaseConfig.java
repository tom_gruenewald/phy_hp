

package tgr.phymosis.homepage.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@ComponentScan({"tgr.phymosis.homepage.worker", "tgr.phymosis.homepage.service", "tgr.phymosis.homepage.config" })
@EnableJpaRepositories("tgr.phymosis.homepage.repositories")
public class HomepageDatabaseConfig
{
  @Autowired
  private DataSource dataSource;

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory()
  {
    LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
    entityManagerFactoryBean.setDataSource(dataSource);
    entityManagerFactoryBean.setPackagesToScan("tgr/phymosis/homepage/domain");

    HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
    jpaVendorAdapter.setShowSql(true);
    entityManagerFactoryBean.setJpaVendorAdapter(jpaVendorAdapter);

    return entityManagerFactoryBean;
  }

  @Bean
  public PlatformTransactionManager transactionManager()
  {
    return new JpaTransactionManager();
  }
}
