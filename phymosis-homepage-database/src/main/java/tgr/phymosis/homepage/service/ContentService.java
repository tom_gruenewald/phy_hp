package tgr.phymosis.homepage.service;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.ContentBean;
import tgr.phymosis.homepage.beans.TokenBean;
import tgr.phymosis.homepage.beans.enums.ContentOrderType;
import tgr.phymosis.homepage.beans.enums.ContentType;
import tgr.phymosis.homepage.beans.enums.MediaType;

import java.util.Date;
import java.util.List;

/**
 * Service performing content related duties like creating, updating, providing or deleting blogposts
 * For all nullable parameters, a null indicates, that no data was found
 */
public interface ContentService
{

  @Nullable
  ContentBean createContent(@NonNull Long authorId,
                            @NonNull ContentType contentType,
                            @Nullable MediaType mediaType,
                            @NonNull Date creationDate,
                            @Nullable String title,
                            @Nullable String text);

  @Nullable
  ContentBean getContentById(@NonNull Long contentId);

  @Nullable
  List<ContentBean> getAllContent();


  /**
   * Provides content of a specified type
   *
   * @param contentType determines which type of content to be fetched from database
   *
   * @return the matching elements
   */
  @Nullable
  List<ContentBean> getActiveContentByType(@NonNull ContentType contentType,
                                           @Nullable ContentOrderType contentOrderType);


  /**
   * Provides content of a specified type for a pageable presentation
   *
   * @param contentType      determines which type of content to be fetched from database
   * @param page             page to fetch
   * @param size             the size of the page fetch
   * @param contentOrderType determines how the Elements are ordered
   *
   * @return the matching elements for the given page of the given size, ordered
   */
  @Nullable
  List<ContentBean> getContentPageableByType(@NonNull ContentType contentType,
                                             @NonNull Integer page,
                                             @NonNull Integer size,
                                             @Nullable ContentOrderType contentOrderType,
                                             @NonNull Boolean active);

  /**
   * Sets the active flag of the content to false to determine whether
   * it is still regularly provided for the frontend or not.
   * Checks the saved object afterwards if deactivation was successful.
   *
   * @param contentId the unique Id of the content to deactivate
   *
   * @return true if deactivation was successful
   */
  boolean deactivateContentById(@NonNull Long contentId, @NonNull TokenBean tokenBean);

  boolean deactivateContentById(@NonNull Long contentId);


  /**
   * Sets the active flag of the content to true to determine whether
   * it is still regularly provided for the frontend or not.
   * Checks the saved object afterwards if reactivation was successful.
   *
   * @param contentId the unique Id of the content to reactivate
   *
   * @return the referring ContentBean if reactivation was successful
   */
  @Nullable
  ContentBean reactivateContentById(@NonNull Long contentId, @NonNull TokenBean tokenBean);

  @Nullable
  ContentBean reactivateContentById(@NonNull Long contentId);

  @NonNull
  int getNumberOfActiveContentByType(@NonNull ContentType contentType);

  @NonNull
  int getNumberOfContentByType(@NonNull ContentType contentType);
}
