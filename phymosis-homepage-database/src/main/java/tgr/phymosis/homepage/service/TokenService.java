package tgr.phymosis.homepage.service;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.TokenBean;

/**
 * service for creating, deleting and providing access tokens used to verify if the user has admin rights or not
 */
public interface TokenService {


  /**
   * checks if the user name and password are correct and matching and returns a unique token for the user to perform administrative work
   * @param userName unique user name
   * @param password user password
   *
   * @return unique token valid for 60min
   */
  @Nullable
  TokenBean createToken(@NonNull String userName,
                        @NonNull String password);

  /**
   * checks, if the given token is still valid, if so, the tokens expiration time is reset to 60min
   *
   * @param tokenBean unique user token
   * @return true if valid
   */
  @NonNull
  Boolean checkToken(@NonNull TokenBean tokenBean);

  void deleteExpiredTokens();

  void deleteTokensByUserId(@NonNull Long userId);
}
