package tgr.phymosis.homepage.service;

import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.beans.MediaLinkBean;

import java.util.List;

/**
 * Service performing medialink related duties like creating, updating, providing or deleting
 * For all nullable parameters, a null indicates, that no data was found
 */
public interface MediaLinkService {

  MediaLinkBean createMediaLink(@NonNull Long contentId,
                                @NonNull String mediaLink);

  MediaLinkBean getMediaLinkById(@NonNull Long MediaLinkId);

  List<MediaLinkBean> getAllMediaLinks();

  List<MediaLinkBean> getMediaLinksByContentId(@NonNull Long contentId);

  MediaLinkBean deleteMediaLinkById(@NonNull Long MediaLinkId);

  List<MediaLinkBean> deleteMediaLinksByContentId(@NonNull Long contentId);

}
