package tgr.phymosis.homepage.service.impl;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import tgr.phymosis.homepage.beans.UserBean;
import tgr.phymosis.homepage.service.UserService;
import tgr.phymosis.homepage.worker.UserWorker;

import javax.transaction.Transactional;

import java.util.List;

@Service
@Transactional
public class UserServiceImpl
  implements UserService
{
  private final UserWorker userWorker;

  public UserServiceImpl(UserWorker userWorker)
  {
    this.userWorker = userWorker;
  }

  @Nullable
  public UserBean getUserByNameAndPassword(@NonNull String name,
                                          @NonNull String password)
  {
    return this.userWorker.getUserByNameAndPassword(name, password);
  }

  @Override
  public UserBean getUserById(@NonNull Long userId)
  {
    return this.userWorker.getUserById(userId);
  }

  @Override
  public UserBean getActiveUserById(@NonNull Long userId)
  {
    return this.userWorker.getActiveUserById(userId);
  }

  @Override
  public List<UserBean> getAllUsers()
  {
    return this.userWorker.getAllUsers();
  }

  @Override
  public List<UserBean> getUsersByPage(@NonNull Integer page,
                                       @NonNull Integer size,
                                       @NonNull Boolean getDeactivatedUsers)
  {
    return this.userWorker.getUsersByPage(page, size, getDeactivatedUsers);
  }

  @Override
  public UserBean createUser(@NonNull String userName)
  {
    return this.userWorker.create(userName);
  }

  @Override
  public boolean setUserPassword(@NonNull Long userId,
                                 @NonNull String newPassword,
                                 @Nullable String oldPassword)
  {
    return this.userWorker.setUserPassword(userId, newPassword, oldPassword);
  }

  @Override
  public boolean deactivateUserById(@NonNull List<Long> userIds)
  {
    boolean success = !userIds.isEmpty();
    for(Long userId : userIds)
    {
      if(!this.deactivateUserById(userId))
      {
        success = false;
      }
    }
    return success;
  }

  @Override
  public boolean deactivateUserById(@NonNull Long userIds)
  {
    return this.userWorker.deactivateUserById(userIds);
  }
}
