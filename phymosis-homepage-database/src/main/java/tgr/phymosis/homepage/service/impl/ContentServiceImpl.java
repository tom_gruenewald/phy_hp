package tgr.phymosis.homepage.service.impl;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import tgr.phymosis.homepage.beans.ContentBean;
import tgr.phymosis.homepage.beans.TokenBean;
import tgr.phymosis.homepage.beans.enums.ContentOrderType;
import tgr.phymosis.homepage.beans.enums.ContentType;
import tgr.phymosis.homepage.beans.enums.MediaType;
import tgr.phymosis.homepage.service.ContentService;
import tgr.phymosis.homepage.worker.ContentWorker;
import tgr.phymosis.homepage.worker.TokenWorker;

import javax.transaction.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ContentServiceImpl
  implements ContentService
{

  private final ContentWorker contentWorker;
  private final TokenWorker tokenWorker;

  public ContentServiceImpl(ContentWorker contentWorker, TokenWorker tokenWorker)
  {
    this.contentWorker = contentWorker;
    this.tokenWorker = tokenWorker;
  }

  @Override
  public ContentBean createContent(@NonNull Long authorId,
                                   @NonNull ContentType contentType,
                                   @Nullable MediaType mediaType,
                                   @NonNull Date creationDate,
                                   @Nullable String title,
                                   @Nullable String text)
  {
    return this.contentWorker.createContent(authorId,
                                            contentType,
                                            mediaType,
                                            creationDate,
                                            title,
                                            text);
  }

  @Override
  public ContentBean getContentById(Long contentId)
  {
    return this.contentWorker.getContentById(contentId);
  }

  @Override
  public List<ContentBean> getAllContent()
  {
    return this.contentWorker.getAllContents();
  }

  @Override
  public List<ContentBean> getActiveContentByType(ContentType contentType,
                                                  ContentOrderType contentOrderType)
  {
    return this.contentWorker.getActiveContentByType(contentType,
                                                     contentOrderType);
  }

  @Override
  public List<ContentBean> getContentPageableByType(@NonNull ContentType contentType,
                                                    @NonNull Integer page,
                                                    @NonNull Integer size,
                                                    @NonNull ContentOrderType contentOrderType,
                                                    @NonNull Boolean active)
  {
    return this.contentWorker.getContentPageableByType(contentType,
                                                       page,
                                                       size,
                                                       contentOrderType,
                                                       active);
  }

  @Override
  public boolean deactivateContentById(@NonNull Long contentId, @NonNull TokenBean tokenBean)
  {
    if(this.tokenWorker.checkToken(tokenBean))
    {
      return this.contentWorker.deactivateContentById(contentId);
    }
    else
    {
      return false;
    }
  }

  @Override
  public ContentBean reactivateContentById(@NonNull Long contentId, @NonNull TokenBean tokenBean)
  {
    if(this.tokenWorker.checkToken(tokenBean))
    {
      return this.contentWorker.reactivateContentById(contentId);
    }
    else
    {
      return null;
    }
  }

  @Override
  public boolean deactivateContentById(@NonNull Long contentId)
  {
    return this.contentWorker.deactivateContentById(contentId);
  }

  @Override
  public ContentBean reactivateContentById(@NonNull Long contentId)
  {
    return this.contentWorker.reactivateContentById(contentId);
  }

  @Override
  public int getNumberOfActiveContentByType(ContentType contentType)
  {
    return this.contentWorker.getNumberOfActiveContentByType(contentType);
  }

  @Override
  public int getNumberOfContentByType(ContentType contentType)
  {
    return this.contentWorker.getNumberOfContentByType(contentType);
  }
}
