package tgr.phymosis.homepage.service;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.TourdateBean;

import java.util.Date;
import java.util.List;

/**
 * Service performing tourdate related duties like creating, updating, providing or deleting
 * For all nullable parameters, a null indicates, that no data was found
 */
public interface TourdateService {

  @Nullable
  TourdateBean createTourdate(@Nullable String location,
                              @NonNull Date date,
                              @NonNull String title,
                              @Nullable String googleMapsLink,
                              @Nullable String text);

  @Nullable
  TourdateBean getTourdateById(@NonNull Long tourdateId);

  @Nullable
  List<TourdateBean> getAllTourdates();

  /**
   * Gets tourdates prepared for a pageable, ordered by date
   *
   * @param page selected of the pageable
   * @param size of the page
   * @return a List of Tourdates in the requested size
   */
  @Nullable
  List<TourdateBean> getTourdatesPage(@NonNull Integer page,
                                      @NonNull Integer size);

  boolean deactivateTourdateById(@NonNull Long tourdateId);

  /**
   * Reactivates the given tourdate
   *
   * @param tourdateId Long id of the tourdate
   * @return the given tourdate entry as a {@link TourdateBean} or null if reactivation failed
   */
  TourdateBean reactivateTourdateById(@NonNull Long tourdateId);

  @NonNull
  List<TourdateBean> getAllActiveTourdates();

  /**
   * Gets active tourdates prepared for a pageable, ordered by date
   *
   * @param page selected of the pageable
   * @param size of the page
   * @return a List of Tourdates in the requested size
   */
  @Nullable
  List<TourdateBean> getActiveTourdatesPage(@NonNull Integer page,
                                            @NonNull Integer size);

  @NonNull
  List<TourdateBean> getPastTourDatesPage(Integer page, Integer size);

  List<TourdateBean> getUpcomingTourDatesPage(Integer page, Integer size);
}
