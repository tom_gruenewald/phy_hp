package tgr.phymosis.homepage.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.lang.NonNull;
import org.springframework.mail.MailAuthenticationException;
import org.springframework.mail.MailParseException;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.stereotype.Service;
import tgr.phymosis.homepage.beans.MailBean;
import tgr.phymosis.homepage.beans.enums.MailErrorType;
import tgr.phymosis.homepage.service.MailService;
import tgr.phymosis.homepage.util.MailBeanUtil;

import java.util.Properties;

@Service
public class MailServiceImpl
  implements MailService
{

  @Autowired
  private JavaMailSender getJavaMailSender;

  /**
   * Tries to send a given email via java mail sender
   *
   * @param from    author
   * @param to      recipient
   * @param title   email's subject
   * @param content text content, basic string formatting like "\n" only
   *
   * @return sent mailBean on success, if sending fails it returns a MailBean with the following data:
   * to = "error";
   * from = "error";
   * title = MailErrorType as string;
   * text = thrown exception as string;
   */
  @Override
  @NonNull
  public MailBean sendEmail(String from, String to, String title, String content)
  {

    SimpleMailMessage message = new SimpleMailMessage();
    try
    {
      message.setFrom(from);
      message.setTo(to);
      message.setSubject(title);
      message.setText(content);
      message.setReplyTo(this.getReplyTo(content));
      getJavaMailSender.send(message);
    }
    catch (MailParseException ex)
    {
      message.setFrom("error");
      message.setTo("error");
      message.setSubject(MailErrorType.PARSE.toString());
      message.setText(ex.toString());
    }
    catch (MailAuthenticationException ex)
    {

      message.setFrom("error");
      message.setTo("error");
      message.setSubject(MailErrorType.AUTHENTICATION.toString());
      message.setText(ex.toString());
    }
    catch(MailSendException ex)
    {

      message.setFrom("error");
      message.setTo("error");
      message.setSubject(MailErrorType.SEND.toString());
      message.setText(ex.toString());
    }
    return MailBeanUtil.convert(message);
  }

  @Bean
  public JavaMailSender getJavaMailSender()
  {
    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
    mailSender.setHost("mail.gmx.net");
    mailSender.setPort(587);

    mailSender.setUsername("phymosis-homepage-notifier@gmx.de");
    mailSender.setPassword("xFL_?259QrStPym");

    Properties props = mailSender.getJavaMailProperties();
    props.put("mail.transport.protocol", "smtp");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.starttls.enable", "true");
    props.put("mail.debug", "true");

    return mailSender;
  }

  private String getReplyTo(String content) {
    int lastIndex = content.lastIndexOf("to ");
    return content.substring(lastIndex + 3);
  }
}
