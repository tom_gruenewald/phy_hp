package tgr.phymosis.homepage.service.impl;

import org.springframework.stereotype.Service;
import tgr.phymosis.homepage.beans.MediaLinkBean;
import tgr.phymosis.homepage.service.MediaLinkService;
import tgr.phymosis.homepage.worker.MediaLinkWorker;

import javax.transaction.Transactional;

import java.util.List;

@Service
@Transactional
public class MediaLinkServiceImpl implements MediaLinkService {
  private final MediaLinkWorker mediaLinkWorker;

  public MediaLinkServiceImpl(MediaLinkWorker mediaLinkWorker)
  {
    this.mediaLinkWorker = mediaLinkWorker;
  }

  @Override
  public MediaLinkBean createMediaLink(Long contentId, String mediaLink)
  {
    return this.mediaLinkWorker.createMediaLink(contentId, mediaLink);
  }

  @Override
  public MediaLinkBean getMediaLinkById(Long MediaLinkId)
  {
    return this.mediaLinkWorker.getMediaLinkById(MediaLinkId);
  }

  @Override
  public List<MediaLinkBean> getAllMediaLinks()
  {
    return this.mediaLinkWorker.getAllMediaLinks();
  }

  @Override
  public List<MediaLinkBean> getMediaLinksByContentId(Long contentId)
  {
    return this.mediaLinkWorker.getMediaLinksByContentId(contentId);
  }

  @Override
  public MediaLinkBean deleteMediaLinkById(Long MediaLinkId)
  {
    return this.mediaLinkWorker.deleteMediaLinkById(MediaLinkId);
  }

  @Override
  public List<MediaLinkBean> deleteMediaLinksByContentId(Long contentId)
  {
    return this.mediaLinkWorker.deleteMediaLinksByContentId(contentId);
  }
}
