package tgr.phymosis.homepage.service.impl;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import tgr.phymosis.homepage.beans.TokenBean;
import tgr.phymosis.homepage.service.TokenService;
import tgr.phymosis.homepage.worker.TokenWorker;

import javax.transaction.Transactional;


@Service
@Transactional
public class TokenServiceImpl implements TokenService {

  private final TokenWorker tokenWorker;

  public TokenServiceImpl(TokenWorker tokenWorker)
  {
    this.tokenWorker = tokenWorker;
  }

  @Override
  public TokenBean createToken(String userName, String password)
  {
    return this.tokenWorker.createToken(userName, password);
  }

  @Override
  public Boolean checkToken(TokenBean tokenBean)
  {
    return this.tokenWorker.checkToken(tokenBean);
  }

    @Override
    public void deleteExpiredTokens() {
        this.tokenWorker.deleteExpiredTokens();
    }

  @Override
  public void deleteTokensByUserId(@NonNull Long userId)
  {
    this.tokenWorker.deleteTokensByUserId(userId);
  }
}
