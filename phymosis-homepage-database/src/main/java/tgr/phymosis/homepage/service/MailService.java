package tgr.phymosis.homepage.service;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.MailBean;

/**
 * Service for sending emails via SMPT
 */
public interface MailService {


  /**
   * Generates a simple mail message, sends and returns it
   *
   * @param from author
   * @param to recipient
   * @param title email's subject
   * @param content text content, basic string formatting like "\n" only
   * @return {@link MailBean}
   */
  MailBean sendEmail(@NonNull String from,
                     @NonNull String to,
                     @Nullable String title,
                     @Nullable String content);
}
