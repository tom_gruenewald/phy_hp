package tgr.phymosis.homepage.service.impl;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import tgr.phymosis.homepage.beans.TourdateBean;
import tgr.phymosis.homepage.service.TourdateService;
import tgr.phymosis.homepage.worker.TourdateWorker;

import javax.transaction.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class TourdateServiceImpl implements TourdateService {

  private final TourdateWorker tourdateWorker;

  public TourdateServiceImpl(TourdateWorker tourdateWorker)
  {
    this.tourdateWorker = tourdateWorker;
  }

  @Override
  public TourdateBean createTourdate(String location, Date date, String title, String googleMapsLink, String text)
  {
    return this.tourdateWorker.createTourdate(location,
                                              date,
                                              title,
                                              googleMapsLink,
                                              text);
  }

  @Override
  public TourdateBean getTourdateById(Long tourdateId)
  {
    return this.tourdateWorker.getTourdateById(tourdateId);
  }


  @Override
  public List<TourdateBean> getAllTourdates()
  {
    return this.tourdateWorker.getAllTourdates();
  }

  @Override
  public List<TourdateBean> getTourdatesPage(Integer page, Integer size)
  {
    return this.tourdateWorker.getTourdatesPage(page, size);
  }

  @Override
  public boolean deactivateTourdateById(@NonNull Long tourdateId)
  {
    return this.tourdateWorker.deactivateTourdateById(tourdateId);
  }

  @Override
  public TourdateBean reactivateTourdateById(@NonNull Long tourdateId)
  {
    return this.tourdateWorker.reactivateTourdateById(tourdateId);
  }

  @Override
  public List<TourdateBean> getAllActiveTourdates()
  {
    return this.tourdateWorker.getAllActiveTourdates();
  }

  @Override
  public List<TourdateBean> getActiveTourdatesPage(Integer page,
                                                   Integer size)
  {
    return this.tourdateWorker.getActiveTourdatesPage(page,
                                                      size);
  }

    @Override
    public List<TourdateBean> getPastTourDatesPage(Integer page, Integer size)
    {
        return this.tourdateWorker.getPastTourdatesPage(page, size);
    }

  @Override
  public List<TourdateBean> getUpcomingTourDatesPage(Integer page, Integer size)
  {
    return this.tourdateWorker.getUpcomingTourdatesPage(page, size);
  }
}
