package tgr.phymosis.homepage.service;

import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.beans.SocialMediaLinkBean;
import tgr.phymosis.homepage.beans.enums.SocialMediaSite;

public interface SocialMediaLinkService {

  SocialMediaLinkBean createSocialMediaLink(@NonNull Long contentId,
                                            @NonNull String mediaLink,
                                            @NonNull SocialMediaSite socialMediaSite);

  SocialMediaLinkBean getSocialMediaLinkByContentId(@NonNull Long contentId);

  SocialMediaLinkBean deactivateSocialMediaLinkByContentId(@NonNull Long contentId);

  SocialMediaLinkBean changeSocialMediaLink(@NonNull Long contentId,
                                            @NonNull String mediaLink,
                                            @NonNull SocialMediaSite socialMediaSite);
}
