package tgr.phymosis.homepage.service.impl;

import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import tgr.phymosis.homepage.beans.SocialMediaLinkBean;
import tgr.phymosis.homepage.beans.enums.SocialMediaSite;
import tgr.phymosis.homepage.service.SocialMediaLinkService;
import tgr.phymosis.homepage.worker.SocialMediaLinkWorker;

import javax.transaction.Transactional;

@Service
@Transactional
public class SocialMediaLinkServiceImpl implements SocialMediaLinkService {

  private final SocialMediaLinkWorker socialMediaLinkWorker;

  public SocialMediaLinkServiceImpl(SocialMediaLinkWorker socialMediaLinkWorker)
  {
    this.socialMediaLinkWorker = socialMediaLinkWorker;
  }

  @Override
  public SocialMediaLinkBean createSocialMediaLink(@NonNull Long contentId,
                                                   @NonNull String mediaLink,
                                                   @NonNull SocialMediaSite socialMediaSite)
  {
    return this.socialMediaLinkWorker.createSocialMediaLink(contentId, mediaLink, socialMediaSite);
  }

  @Override
  public SocialMediaLinkBean getSocialMediaLinkByContentId(@NonNull Long contentId)
  {
    return this.socialMediaLinkWorker.getByContentId(contentId);
  }

  @Override
  public SocialMediaLinkBean deactivateSocialMediaLinkByContentId(@NonNull Long contentId)
  {
    return this.socialMediaLinkWorker.deactivateSocialMediaLinkByContentId(contentId);
  }

  @Override
  public SocialMediaLinkBean changeSocialMediaLink(@NonNull Long contentId,
                                                   @NonNull String mediaLink,
                                                   @NonNull SocialMediaSite socialMediaSite)
  {
    return this.socialMediaLinkWorker.changeSocialMediaLink(contentId, mediaLink, socialMediaSite);
  }
}
