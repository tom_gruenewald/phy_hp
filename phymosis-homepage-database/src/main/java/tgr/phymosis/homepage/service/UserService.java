package tgr.phymosis.homepage.service;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.UserBean;

import java.util.List;

/**
 * Service performing all user related work, like creating,
 * deleting and providing user data.
 * For all nullable parameters, a null indicates, that no data was found
 */
public interface UserService
{

  @Nullable
  UserBean getUserByNameAndPassword(@NonNull String name,
                                    @NonNull String password);

  @Nullable
  UserBean getUserById(@NonNull Long userId);

  @Nullable
  UserBean getActiveUserById(@NonNull Long userId);

  /**
   * Returns a UserBean for every user row available in the database
   *
   * @return {@link List<UserBean>} ordered by id ascending or null if no entries where found
   */
  @Nullable
  List<UserBean> getAllUsers();

  /**
   * Returns a certain number of UserBeans from the database
   * starting with the given position in the "PHP_USER" table
   *
   * @return {@link List<UserBean>} ordered by id ascending
   */
  @Nullable
  List<UserBean> getUsersByPage(@NonNull Integer page,
                                @NonNull Integer size,
                                @NonNull Boolean getDeactivatedUsers);

  @Nullable
  UserBean createUser(@NonNull String userName);

  /**
   * Changes the given users passphrase, if the old passphrase is correct.
   * If no passphrase has been set for the user yet oldPassword needs to be null.
   *
   * @param userId of the user to change
   * @param newPassword new passphrase
   * @param oldPassword null if not set yet
   * @return true if changed successfully
   */
  @NonNull
  boolean setUserPassword(@NonNull Long userId,
                          @NonNull String newPassword,
                          @Nullable String oldPassword);

  /**
   * deactivate all users with the ids in the given list.
   *
   * @param userIds {@link List<Long>} of ids referring to users in the database
   * @return false if not found or not deactivated correctly
   */
  @NonNull
  boolean deactivateUserById(@NonNull List<Long> userIds);

  /**
   * deactivate the user with the given id.
   *
   * @param userIds {@link Long} of ids referring to users in the database
   * @return false if not found or not deactivated correctly
   */
  @NonNull
  boolean deactivateUserById(@NonNull Long userIds);

}
