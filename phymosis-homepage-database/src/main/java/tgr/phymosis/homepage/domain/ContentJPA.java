package tgr.phymosis.homepage.domain;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.enums.ContentType;
import tgr.phymosis.homepage.beans.enums.MediaType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.util.Date;

@Entity
@Table(name = "PHP_CONTENT")
public class ContentJPA
{

  @Nullable
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @NonNull
  @ManyToOne
  @JoinColumn(name = "AUTHOR_ID", referencedColumnName = "ID", nullable = false, updatable = false)
  private UserJPA author;

  @NonNull
  @Enumerated(value = EnumType.STRING)
  @Column(name = "CONTENT_TYPE", nullable = false)
  private ContentType contentType;

  @Nullable
  @Enumerated(value = EnumType.STRING)
  @Column(name = "MEDIA_TYPE")
  private MediaType mediaType;

  @NonNull
  @Column(name = "CREATION_DATE", nullable = false)
  private Date creationDate;

  @Nullable
  @Column(name = "TITLE")
  private String title;

  @Nullable
  @Column(name = "TEXT")
  private String text;

  @Nullable
  @Column(name = "ACTIVE", nullable = false)
  private Boolean active;

  public ContentJPA()
  {
    // serialization
  }

  public ContentJPA(@NonNull UserJPA author,
                    @NonNull ContentType contentType,
                    @Nullable MediaType mediaType,
                    @NonNull Date creationDate,
                    @Nullable String title,
                    @Nullable String text)
  {
   this.author = author;
   this.contentType = contentType;
   this.mediaType = mediaType;
   this.creationDate = creationDate;
   this.title = title;
   this.text = text;
   this.active = true;
  }

  @NonNull
  public UserJPA getAuthor()
  {
    return author;
  }

  @Nullable
  public Long getId()
  {
    return id;
  }

  @NonNull
  public ContentType getContentType()
  {
    return contentType;
  }

  @Nullable
  public MediaType getMediaType()
  {
    return mediaType;
  }

  @NonNull
  public Date getCreationDate()
  {
    return creationDate;
  }

  @Nullable
  public String getTitle()
  {
    return title;
  }

  @Nullable
  public String getText()
  {
    return text;
  }

  @Nullable
  public Boolean getActive()
  {
    return active;
  }

  public void setContentType(@NonNull ContentType contentType)
  {
    this.contentType = contentType;
  }

  public void setMediaType(@Nullable MediaType mediaType)
  {
    this.mediaType = mediaType;
  }

  public void setCreationDate(@NonNull Date created)
  {
    this.creationDate = created;
  }

  public void setTitle(@Nullable String title)
  {
    this.title = title;
  }

  public void setText(@Nullable String text)
  {
    this.text = text;
  }

  public void setActive(@Nullable Boolean active)
  {
    this.active = active;
  }
}
