package tgr.phymosis.homepage.domain;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PHP_EMAIL_ADDRESS")
public class EmailAddressJPA
{

  @Nullable
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @NonNull
  @ManyToOne
  @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false, updatable = false)
  private UserJPA user;

  @NonNull
  @Column(name = "EMAIL_ADDRESS", nullable = false)
  private String emailAddress;

  @NonNull
  @Column(name = "ACTIVE", nullable = false)
  private Boolean active;

  public EmailAddressJPA()
  {
    // serializable
  }

  public EmailAddressJPA(@NonNull UserJPA user,
                         @NonNull String emailAddress)
  {
    this.user = user;
    this.emailAddress = emailAddress;
    this.active = true;
  }

  @Nullable
  public Long getId()
  {
    return id;
  }

  @NonNull
  public UserJPA getUser()
  {
    return user;
  }

  @NonNull
  public String getEmailAddress()
  {
    return emailAddress;
  }

  @NonNull
  public Boolean getActive()
  {
    return active;
  }

  public void setEmailAddress(@NonNull String emailAddress)
  {
    this.emailAddress = emailAddress;
  }

  public void setActive(@NonNull Boolean active)
  {
    this.active = active;
  }
}
