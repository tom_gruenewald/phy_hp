package tgr.phymosis.homepage.domain;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.util.Date;

@Entity
@Table(name = "PHP_TOKEN")
public class TokenJPA {

  @Nullable
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @NonNull
  @Column(name = "USER_ID", nullable = false, updatable = false)
  private Long userId;

  @NonNull
  @Column(name = "TOKEN", nullable = false, unique = true, updatable = false)
  private String token;

  @NonNull
  @Column(name = "VALIDATED", nullable = false)
  private Date validated;

  public TokenJPA()
  {
    // serialization
  }

  public TokenJPA(@NonNull String token,
                  @NonNull Long userId,
                  @NonNull Date validated)
  {
    this.token = token;
    this.userId = userId;
    this.validated = validated;
  }

  @Nullable
  public Long getId()
  {
    return id;
  }

  @NonNull
  public Long getUserId()
  {
    return userId;
  }

  @NonNull
  public String getToken()
  {
    return token;
  }

  @NonNull
  public Date getValidated()
  {
    return validated;
  }

  public void setValidated(@NonNull Date validated)
  {
    this.validated = validated;
  }
}
