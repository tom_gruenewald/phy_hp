package tgr.phymosis.homepage.domain;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PHP_MEDIA_LINK")
public class MediaLinkJPA
{

  @Nullable
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @NonNull
  @Column(name = "CONTENT_ID", nullable = false, updatable = false)
  private Long contentId;

  @NonNull
  @Column(name = "MEDIA_LINK", nullable = false)
  private String mediaLink;

  @NonNull
  @Column(name = "ACTIVE", nullable = false)
  private Boolean active;

  public MediaLinkJPA()
  {
    // serialization
  }

  public MediaLinkJPA(@NonNull Long contentId,
                      @NonNull String mediaLink)
  {
    this.contentId = contentId;
    this.mediaLink = mediaLink;
    this.active = true;
  }

  @Nullable
  public Long getId()
  {
    return id;
  }

  @NonNull
  public Long getContentId()
  {
    return contentId;
  }

  @NonNull
  public String getMediaLink()
  {
    return mediaLink;
  }

  @NonNull
  public Boolean getActive()
  {
    return active;
  }

  public void setMediaLink(@NonNull String mediaLink)
  {
    this.mediaLink = mediaLink;
  }

  public void setActive(@NonNull Boolean active)
  {
    this.active = active;
  }
}
