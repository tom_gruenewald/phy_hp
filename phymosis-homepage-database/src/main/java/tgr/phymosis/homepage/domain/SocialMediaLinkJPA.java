package tgr.phymosis.homepage.domain;


import org.springframework.lang.NonNull;
import tgr.phymosis.homepage.beans.enums.SocialMediaSite;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PHP_SOCIAL_MEDIA_LINK")
public class SocialMediaLinkJPA
{

  @NonNull
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @NonNull
  @Column(name = "CONTENT_ID", nullable = false, updatable = false, unique = true)
  private Long contentId;

  @NonNull
  @Column(name = "MEDIA_LINK", nullable = false)
  private String mediaLink;

  @NonNull
  @Enumerated(value = EnumType.STRING)
  @Column(name = "SOCIAL_MEDIA_SITE", nullable = false)
  private SocialMediaSite socialMediaSite;

  @NonNull
  @Column(name = "ACTIVE", nullable = false)
  private Boolean active;

  public SocialMediaLinkJPA()
  {
    // serialization
  }

  public SocialMediaLinkJPA(@NonNull Long contentId,
                            @NonNull String mediaLink,
                            @NonNull SocialMediaSite socialMediaSite)
  {
    this.active = true;
    this.contentId = contentId;
    this.mediaLink = mediaLink;
    this.socialMediaSite = socialMediaSite;
  }

  @NonNull
  public Long getId()
  {
    return id;
  }

  @NonNull
  public Long getContentId()
  {
    return contentId;
  }

  @NonNull
  public String getMediaLink()
  {
    return mediaLink;
  }

  @NonNull
  public SocialMediaSite getSocialMediaSite()
  {
    return socialMediaSite;
  }

  @NonNull
  public Boolean getActive()
  {
    return active;
  }

  public void setMediaLink(@NonNull String mediaLink)
  {
    this.mediaLink = mediaLink;
  }

  public void setSocialMediaSite(@NonNull SocialMediaSite socialMediaSite)
  {
    this.socialMediaSite = socialMediaSite;
  }

  public void setActive(@NonNull Boolean active)
  {
    this.active = active;
  }
}
