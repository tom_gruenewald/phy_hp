package tgr.phymosis.homepage.domain;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.util.Date;

@Entity
@Table(name = "PHP_TOURDATE")
public class TourdateJPA {

  @Nullable
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Nullable
  @Column(name = "LOCATION")
  private String location;

  @NonNull
  @Column(name = "DATE", nullable = false)
  private Date date;

  @NonNull
  @Column(name = "TITLE", nullable = false)
  private String title;

  @Nullable
  @Column(name = "GOOGLE_MAPS_LINK")
  private String googleMapsLink;

  @Nullable
  @Column(name = "TEXT")
  private String text;

  @NonNull
  @Column(name = "ACTIVE", nullable = false)
  private Boolean active;

  public TourdateJPA()
  {
    // serialization
  }

  public TourdateJPA(@Nullable String location,
                     @NonNull Date date,
                     @NonNull String title,
                     @Nullable String googleMapsLink,
                     @Nullable String text)
  {
    this.location = location;
    this.date = date;
    this.title = title;
    this.googleMapsLink = googleMapsLink;
    this.text = text;
    this.active = true;
  }

  @Nullable
  public Long getId()
  {
    return id;
  }

  @Nullable
  public String getLocation()
  {
    return location;
  }

  @NonNull
  public Date getDate()
  {
    return date;
  }

  @NonNull
  public String getTitle()
  {
    return title;
  }

  @Nullable
  public String getGoogleMapsLink()
  {
    return googleMapsLink;
  }

  @Nullable
  public String getText()
  {
    return text;
  }

  @NonNull
  public Boolean getActive()
  {
    return active;
  }

  public void setLocation(@Nullable String location)
  {
    this.location = location;
  }

  public void setDate(@NonNull Date date)
  {
    this.date = date;
  }

  public void setTitle(@NonNull String title)
  {
    this.title = title;
  }

  public void setGoogleMapsLink(@Nullable String googleMapsLink)
  {
    this.googleMapsLink = googleMapsLink;
  }

  public void setText(@Nullable String text)
  {
    this.text = text;
  }

  public void setActive(@NonNull Boolean active)
  {
    this.active = active;
  }
}
