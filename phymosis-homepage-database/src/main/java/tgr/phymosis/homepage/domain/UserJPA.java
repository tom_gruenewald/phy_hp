package tgr.phymosis.homepage.domain;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import tgr.phymosis.homepage.beans.enums.SecurityLevel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PHP_USER")
public class UserJPA
{
  @Nullable
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @NonNull
  @Column(name = "NAME", nullable = false, unique = true)
  private String name;

  @Nullable
  @Column(name = "PASSWORD")
  private String password;

  @NonNull
  @Enumerated(value = EnumType.STRING)
  @Column(name = "SEC_LEVEL")
  private SecurityLevel securityLevel;

  @NonNull
  @Column(name = "ACTIVE", nullable = false)
  private boolean active;

  public UserJPA()
  {
    // serialization
  }

  public UserJPA(@NonNull String name)
  {
    this.name = name;
    this.securityLevel = SecurityLevel.NONE;
    this.password = null;
    this.active = true;
  }

  public UserJPA(@NonNull String name,
                 @NonNull String password)
  {
    this.name = name;
    this.password = password;
    this.securityLevel = SecurityLevel.NONE;
    this.active = true;
  }

  public UserJPA(@NonNull String name,
                 @NonNull String password,
                 @NonNull SecurityLevel securityLevel)
  {
    this.name = name;
    this.password = password;
    this.securityLevel = securityLevel;
    this.active = true;
  }

  @Nullable
  public long getId()
  {
    return id;
  }

  @NonNull
  public String getName()
  {
    return name;
  }

  @Nullable
  public String getPassword()
  {
    return password;
  }

  @NonNull
  public SecurityLevel getSecurityLevel()
  {
    return securityLevel;
  }

  @NonNull
  public boolean isActive()
  {
    return active;
  }

  public void setName(@NonNull String name)
  {
    this.name = name;
  }

  public void setPassword(@NonNull String passphrase)
  {
    this.password = passphrase;
  }

  public void setSecurityLevel(@NonNull SecurityLevel secLevel)
  {
    this.securityLevel = secLevel;
  }

  public void setActive(@NonNull boolean active)
  {
    this.active = active;
  }
}
