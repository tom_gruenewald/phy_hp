package tgr.phymosis.homepage.webapp;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import tgr.phymosis.homepage.config.RestConfig;

@SpringBootApplication
@EnableScheduling
@Import(RestConfig.class)
public class PhymosisHomepageApplication
  extends SpringBootServletInitializer
{
  public static void main(String[] args)
  {
    configureApplication(new SpringApplicationBuilder()).run(args);
  }

  @Override
  protected SpringApplicationBuilder configure(SpringApplicationBuilder application)
  {
    return configureApplication(application);
  }

  private static SpringApplicationBuilder configureApplication(SpringApplicationBuilder application)
  {
    return application.sources(PhymosisHomepageApplication.class);
  }
}
