/*
 * Copyright (c) 2017 conLeos GmbH. All Rights reserved.
 *
 * This software is the confidential intellectual property of conLeos GmbH; it is copyrighted and licensed.
 */
package tgr.phymosis.homepage.webapp.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@Profile("development")
public class WebMvcConfigurerDevelopment
  implements WebMvcConfigurer
{
  @Override
  public void addCorsMappings(CorsRegistry registry)
  {
    registry
      .addMapping("/**")
      .allowedMethods("*")
      .allowedOrigins("*")
      .allowedHeaders("*")
      .allowCredentials(true);
  }
}

