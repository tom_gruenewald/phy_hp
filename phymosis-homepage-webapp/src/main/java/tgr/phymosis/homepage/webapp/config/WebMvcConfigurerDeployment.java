/*
 * Copyright (c) 2017 conLeos GmbH. All Rights reserved.
 *
 * This software is the confidential intellectual property of conLeos GmbH; it is copyrighted and licensed.
 */
package tgr.phymosis.homepage.webapp.config;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.StreamUtils;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.resource.AbstractResourceResolver;
import org.springframework.web.servlet.resource.ResourceResolverChain;
import org.springframework.web.servlet.resource.TransformedResource;

@Configuration
@Profile("deployment")
public class WebMvcConfigurerDeployment
  implements WebMvcConfigurer
{
  private static final Logger LOG = LoggerFactory.getLogger(WebMvcConfigurerDeployment.class);
  private static final String INDEX_HTML = "index.html";
  private static final String ROOT_URL = "/";

  private final ServletContext servletContext;
  private final RoutingProperties routingProperties;

  private final Resource indexHtml;

  @Autowired
  public WebMvcConfigurerDeployment(ServletContext servletContext, RoutingProperties routingProperties)
  {
    this.servletContext = servletContext;
    this.routingProperties = routingProperties;
    this.indexHtml = new ClassPathResource(routingProperties.getStaticSourcesPath() + INDEX_HTML);
  }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry)
  {
    // add a default resolver to rootMapping (defaults to /)
    registry.addResourceHandler(routingProperties.getRootMapping(), routingProperties.getRootMapping() + "**")
      .resourceChain(true)
      .addResolver(new StaticResourceResolver(getPatchedIndex(), routingProperties.getStaticSourcesPath()));
  }

  private Resource getPatchedIndex()
  {
    try (InputStream inputStream = indexHtml.getInputStream();
         ByteArrayOutputStream buffer = new ByteArrayOutputStream())
    {
      StreamUtils.copy(inputStream, buffer);
      String unpatchedHtml = new String(buffer.toByteArray());

      // inject the context path into index.html resource
      String baseHref = servletContext.getContextPath() + routingProperties.getRootMapping();
      String withBaseHref = injectBaseHref(unpatchedHtml, baseHref);

      // inject the defined custom injections
      String withInjections = injectCustom(withBaseHref, routingProperties.getInjections());
      // keep in memory as TransformedResource, which preserves the original resource's other information
      return new TransformedResource(indexHtml, withInjections.getBytes());
    }
    catch(IOException e)
    {
      LOG.error("could not patch index resource", e);
    }

    return indexHtml;
  }

  // <base href="HERE GOES THE CONTEXT PATH">
  private String injectBaseHref(String unpatchedHtml, String baseHref)
  {
    Matcher matcher = Pattern.compile("<base.*?href=\"(.*?)\".*?>").matcher(unpatchedHtml);
    if(!matcher.find())
    {
      return unpatchedHtml;
    }

    return unpatchedHtml.substring(0, matcher.start(1))
           + baseHref
           + unpatchedHtml.substring(matcher.end(1), unpatchedHtml.length());
  }

  // custom injections for config files / tracking etc.
  private String injectCustom(String unpatchedHtml, List<Injection> injections)
  {
    String patchedHtml = unpatchedHtml;
    for(Injection injection : injections)
    {
      Matcher matcher = Pattern.compile(injection.getHook()).matcher(patchedHtml);
      if(matcher.find())
      {
        try (InputStream inputStream = new ClassPathResource(injection.getSource()).getInputStream();
             ByteArrayOutputStream buffer = new ByteArrayOutputStream())
        {
          StreamUtils.copy(inputStream, buffer);

          patchedHtml = patchedHtml.substring(0, matcher.start())
                        + new String(buffer.toByteArray())
                        + patchedHtml.substring(matcher.end(), patchedHtml.length());
        }
        catch(IOException e)
        {
          LOG.error("could not inject " + injection.getSource(), e);
        }
      }
    }

    return patchedHtml;
  }

  private static class StaticResourceResolver
    extends AbstractResourceResolver
  {
    private final Resource serveAsIndex;
    private final String staticResourcePath;

    private StaticResourceResolver(Resource serveAsIndex, String staticResourcePath)
    {
      this.serveAsIndex = serveAsIndex;
      this.staticResourcePath = staticResourcePath;
    }

    @Override
    protected Resource resolveResourceInternal(HttpServletRequest request, String requestPath, List<? extends Resource> locations, ResourceResolverChain chain)
    {
      return resolve(requestPath);
    }

    @Override
    protected String resolveUrlPathInternal(String resourceUrlPath, List<? extends Resource> locations, ResourceResolverChain chain)
    {
      Resource resolvedResource = resolve(resourceUrlPath);
      if(resolvedResource == null)
      {
        return null;
      }

      try
      {
        return resolvedResource.getURL().toString();
      }
      catch(IOException e)
      {
        return resolvedResource.getFilename();
      }
    }

    // always returns the serveAsIndex, unless the request path was whitelisted
    private Resource resolve(String requestPath)
    {
      if(requestPath == null)
      {
        return null;
      }

      ClassPathResource staticResource = new ClassPathResource(staticResourcePath + requestPath);
      if(staticResource.exists() && !ROOT_URL.equals(requestPath) && !INDEX_HTML.equals(requestPath))
      {
        // whitelisted resources are delegated to the static resource folder on classpath
        return staticResource;
      }

      return serveAsIndex;
    }
  }

  @Configuration
  @ConfigurationProperties("routing")
  public static class RoutingProperties
  {
    private static final String DEFAULT_ROOT_MAPPING = ROOT_URL;
    private static final String DEFAULT_STATIC_SOURCES_PATH = "/static/";

    private String rootMapping;
    private String staticSourcesPath;
    private List<Injection> injections = new ArrayList<>();

    public String getRootMapping()
    {
      return Optional.ofNullable(rootMapping)
        .orElse(DEFAULT_ROOT_MAPPING);
    }

    public void setRootMapping(String rootMapping)
    {
      this.rootMapping = rootMapping;
    }

    public String getStaticSourcesPath()
    {
      return Optional.ofNullable(staticSourcesPath)
        .orElse(DEFAULT_STATIC_SOURCES_PATH);
    }

    public void setStaticSourcesPath(String staticSourcesPath)
    {
      this.staticSourcesPath = staticSourcesPath;
    }

    public List<Injection> getInjections()
    {
      return injections;
    }

    public void setInjections(List<Injection> injections)
    {
      this.injections = injections;
    }
  }

  public static class Injection
  {
    private String hook;
    private String source;

    public String getHook()
    {
      return hook;
    }

    public void setHook(String hook)
    {
      this.hook = hook;
    }

    public String getSource()
    {
      return source;
    }

    public void setSource(String source)
    {
      this.source = source;
    }
  }
}
