module.exports = function (config) {
  config.set({
    frameworks: ['jasmine', 'karma-typescript'],
    plugins: [
      require('karma-jasmine'),
      require('karma-typescript'),
      require('karma-jsdom-launcher'),
      require('karma-spec-reporter')
    ],
    karmaTypescriptConfig: {
      tsconfig: "./src/tsconfig.spec.json",
     reports: {
        "lcovonly": {
          "directory": "dist",
          "filename": "lcov.info",
          "subdirectory": "karma"
        }
      }
    },
    files: [
      "src/**/*.ts"
    ],
    preprocessors: {
      "**/*.ts": ["karma-typescript"]
    },
    reporters: ['spec', 'karma-typescript'],
    browsers: ['jsdom'],
    singleRun: true
  });
};
