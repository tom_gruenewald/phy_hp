import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

const HTTPS = 'https:';
const HTTP = 'http:';

if (environment.restBaseUrl.startsWith(HTTPS)) {
  environment.webSocketBaseUrl = 'wss:' + environment.restBaseUrl.substring(HTTPS.length)+environment.webSocketBaseUrl;
} else if (environment.restBaseUrl.startsWith(HTTP)) {
  environment.webSocketBaseUrl = 'ws:' + environment.restBaseUrl.substring(HTTP.length)+ environment.webSocketBaseUrl;
} else {
  throw new Error('Could not create WebSocket URI: ' + environment.restBaseUrl);
}

if (environment.production) {
  enableProdMode();
}

platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));
