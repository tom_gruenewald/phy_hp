export const environment = {
  production: true,
  restBaseUrl: document.head.baseURI,
  webSocketBaseUrl: '/phy-websocket',
  serverRestPrefix: '/rest/phy',
};
