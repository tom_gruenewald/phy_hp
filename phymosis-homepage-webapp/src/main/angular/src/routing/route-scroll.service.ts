import {Injectable} from '@angular/core';
import {ReplaySubject} from "rxjs";
import {RouteTemplate} from "./route-template";
import {distinctUntilChanged} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class RouteScrollService {

  private readonly $currentRouteAnchorSubject = new ReplaySubject<RouteTemplate>(1);
  public readonly $currentRouteAnchor = this.$currentRouteAnchorSubject
    .asObservable()
    .pipe(
      distinctUntilChanged()
    );

  constructor() {
  }

  public navigateTo(routeTemplate: RouteTemplate): void {
    this.$currentRouteAnchorSubject.next(routeTemplate);
  }
}
