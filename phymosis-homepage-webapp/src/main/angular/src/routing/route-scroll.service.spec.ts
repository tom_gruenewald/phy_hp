import { TestBed } from '@angular/core/testing';

import { RouteScrollService } from './route-scroll.service';

describe('RouteScrollService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RouteScrollService = TestBed.get(RouteScrollService);
    expect(service).toBeTruthy();
  });
});
