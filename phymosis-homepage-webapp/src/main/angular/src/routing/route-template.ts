/**
 * Possible Position Ids to scroll to on the page
 */
export enum RouteTemplate {
  NEWS = 'newsanchor',
  RELEASES = 'releasesanchor',
  TOURDATES = 'tourdatesanchor',
  MEDIA = 'mediaanchor',
  BIO = 'bioanchor',
  CONTACT = 'contactanchor'
}
