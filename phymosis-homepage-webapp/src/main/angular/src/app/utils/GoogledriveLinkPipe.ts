import {Pipe, PipeTransform} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';

@Pipe({
  name: 'googleDriveLink'
})
export class GoogleDriveLinkPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {
  }

  transform(url: string) {
    if (url.includes('/open?'))
      url = url.replace('/open?', '/uc?');
    if (url.includes('drive.google.com/file/d/')) {
      url = url.replace('drive.google.com/file/d/', 'drive.google.com/uc?id=');
      url = url.includes('/view?usp=sharing')
        ? url.replace('/view?usp=sharing', '')
        : url.replace('/view', '')
    }
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}

