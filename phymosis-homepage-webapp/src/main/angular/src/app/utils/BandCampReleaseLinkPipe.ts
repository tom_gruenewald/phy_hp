import {Pipe, PipeTransform} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";

@Pipe({
  name: 'bandCampReleaseLink'
})
export class BandCampReleaseLinkPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {
  }

  transform(url: string) {
    const startIndex = url.indexOf('album=');
    const endIndex = url.indexOf('/', startIndex);
    url = 'https://bandcamp.com/EmbeddedPlayer/'
      + url.slice(startIndex, endIndex)
      + '/size=large/bgcol=ffffff/linkcol=a33a37/artwork=small/transparent=true/';
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
