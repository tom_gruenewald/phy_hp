
/**
 * Checks that the provided value is neither undefined nor null.
 *
 * @param value a value which might be undefined/null
 *
 * @return true if the provided value is not undefined/null
 */
export function isSafe<T>(value: T | null | undefined): boolean {
  return value != undefined
    && value != null;
}
