import {Pipe, PipeTransform} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";

@Pipe({
  name: 'googleMapsLink'
})
export class GoogleMapsLinkPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {
  }

  transform(url: string) {
    const startIndexA = url.indexOf('https://www.google.com/maps/');
    const endIndexA = url.indexOf('"', startIndexA);
    url = url.slice(startIndexA, endIndexA !== 0
      ? endIndexA-1
      : url.length-1);
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

}
