import {Pipe, PipeTransform} from "@angular/core";
import {DomSanitizer} from "@angular/platform-browser";

@Pipe({
  name: 'bandCampSongLink'
})
export class BandCampSongLinkPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {
  }

  transform(url: string) {
    const startIndexA = url.indexOf('album=');
    const endIndexA = url.indexOf('/', startIndexA);
    const startIndexT = url.indexOf('track=');
    const endIndexT = url.indexOf('/', startIndexT);
    url = 'https://bandcamp.com/EmbeddedPlayer/'
      + url.slice(startIndexA, endIndexA)
      + '/size=small/bgcol=ffffff/linkcol=a33a37/'
      + url.slice(startIndexT, endIndexT)
      + '/transparent=true/';
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
