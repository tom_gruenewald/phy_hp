export declare const URL_SECTION_DELIMITER = "/";

/**
 * A minimal typed key/value store.
 */
export interface KeyValueStore<V> {
  [key: string]: V;
}

export interface KeyValuePair<V> {
  key: string;
  value: V;
}

/**
 * Concatenates the specified url sections.
 *
 * @param sections the url sections we want to concatenate (and preceding or trailing URL_SECTION_DELIMITERs are omitted)
 *
 * @return the concatenated url
 */
export function concatUrl(...sections: string[]): string {
  if (sections.length > 1) {
    let outputString = sections[0];
    for (let i = 1; i < sections.length; i++) {
      let section: string;
      sections[i].charAt(0) !== '/'
        ? section = '/' + sections[i]
        : section = sections[i];

      if (section.charAt(section.length - 1) === '/')
        section = section.substr(0, section.length - 1);
      outputString += section;
    }
    return outputString;
  }
  return sections[0];
}

export function stringifyBoolean(value: boolean): string {
  return value ? 'true' : 'false';
}

/**
 * converts a youtube watch link into an embed link,
 * if it is a valid youtube link
 */
export function convertYouTubeLink(originalLink: string): string | null {
  if (!originalLink.includes('youtube.com')
    && !originalLink.includes('youtube-nocookie.com')) {
    return 'The link seems to be no valid youtube embed link, ' +
      'use the share=>embed function below the video and enter the link after \"src\" in the given embed-text.';
  }
  if (!originalLink.includes('/embed/')
    && originalLink.includes('/watch?v=')) {
    return originalLink.replace('/watch?v=', '/embed/');
  }
  return originalLink;
}

