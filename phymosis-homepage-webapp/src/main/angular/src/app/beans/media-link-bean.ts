export interface MediaLinkBean {
  id: number;
  contentId: number;
  mediaLink: string;
}
