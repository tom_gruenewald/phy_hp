export enum SecurityLevel {
  NONE = 'NONE',
  ADMIN = 'ADMIN',
  SUPER_ADMIN = 'SUPER_ADMIN',
  BAND_MEMBER = 'BAND_MEMBER',
}
