export enum ContentOrderType {
  DATE_DESC = 'DATE_DESC',
  DATE_ASC='DATE_ASC',
  TITLE_DESC='TITLE_DESC',
  TITLE_ASC='TITLE_ASC',
}
