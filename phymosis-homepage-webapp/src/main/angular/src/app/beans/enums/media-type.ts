export enum MediaType {
  NONE = 'NONE',
  YOUTUBE_VIDEO = 'YOUTUBE_VIDEO',
  BANDCAMP_LINK = 'BANDCAMP_LINK',
  IMAGE = 'IMAGE',
}
