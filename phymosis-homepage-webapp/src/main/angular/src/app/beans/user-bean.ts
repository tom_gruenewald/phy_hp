import {SecurityLevel} from "./enums/security-level";
import {TokenBean} from "./token-bean";

export interface UserBean {
  id: number;
  name: string;
  securityLevel: SecurityLevel;
  emailAddress: string;
  tokenBean: TokenBean;
  active: boolean;
}
