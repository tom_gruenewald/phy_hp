export interface MailBean {
  to: string;
  from: string;
  title: string;
  text: string;
}
