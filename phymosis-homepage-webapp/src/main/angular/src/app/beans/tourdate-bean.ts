export interface TourdateBean {
  id: number;
  location: string;
  date: Date;
  title: string;
  googleMapsLink: string;
  text: string;
  active: boolean;
}
