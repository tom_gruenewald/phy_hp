export class TokenBean {


  constructor(token: string, expirationTime: Date) {
    this._token = token;
    this._expirationTime = expirationTime;
  }

  private readonly _token: string;
  private readonly _expirationTime: Date;


  public get token(): string {
    return this._token;
  }

  public get expirationTime(): Date {
    return this._expirationTime;
  }

  public static stringify(token:TokenBean): string {
    return token.token + ';' + new Date(token.expirationTime).valueOf().toString();
  }
}
