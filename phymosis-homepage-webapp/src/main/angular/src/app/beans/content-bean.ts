import {ContentType} from "./enums/content-type";
import {MediaType} from "./enums/media-type";

export interface ContentBean {
  id: number;
  authorId: number;
  contentType: ContentType;
  mediaType: MediaType;
  creationDate: Date;
  title: string;
  text: string;
  active: boolean;
}
