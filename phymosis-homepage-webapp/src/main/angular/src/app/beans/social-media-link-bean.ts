import {SocialMediaSite} from "./enums/social-media-site";

export interface SocialMediaLinkBean {
  id: number;
  contentId: number;
  mediaLink: string;
  socialMediaSite: SocialMediaSite;
  active: boolean;
}
