import {AfterViewInit, Component, ElementRef, OnDestroy, ViewChild} from '@angular/core';
import {RouteTemplate} from "../../../routing/route-template";
import {ViewportScroller} from "@angular/common";
import {RouteScrollService} from "../../../routing/route-scroll.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'phy-main-scroll-container',
  templateUrl: './main-scroll-container.component.html',
  styleUrls: ['./main-scroll-container.component.scss']
})
export class MainScrollContainerComponent implements OnDestroy, AfterViewInit {

  @ViewChild('releasesAnchor', {static: false}) releasesAnchor: ElementRef;
  @ViewChild('newsAnchor', {static: false}) newsAnchor: ElementRef;
  @ViewChild('liveAnchor', {static: false}) liveAnchor: ElementRef;
  @ViewChild('contactAnchor', {static: false}) contactAnchor: ElementRef;

  public readonly RouteTemplate = RouteTemplate;
  private routeSubscription: Subscription;

  constructor(private viewportScroller: ViewportScroller,
              private readonly routeScrollService: RouteScrollService) {
  }

  public scrollToElement(elementId: RouteTemplate): void {
    let element: any;
    switch (elementId) {
      case RouteTemplate.NEWS:
        element = this.newsAnchor.nativeElement;
        break;
      case RouteTemplate.RELEASES:
        element = this.releasesAnchor.nativeElement;
        break;
      case RouteTemplate.TOURDATES:
        element = this.liveAnchor.nativeElement;
        break;
      case RouteTemplate.CONTACT:
        element = this.contactAnchor.nativeElement;
        break;
      default:
        element = this.newsAnchor.nativeElement;
        break;
    }
    element.scrollIntoView({ behavior:'smooth'});
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
  }

  ngAfterViewInit(): void {
    this.routeSubscription = this.routeScrollService.$currentRouteAnchor.subscribe(
      routeTemplate => this.scrollToElement(routeTemplate));
  }

}
