import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainScrollContainerComponent } from './main-scroll-container.component';

describe('MainScrollContainerComponent', () => {
  let component: MainScrollContainerComponent;
  let fixture: ComponentFixture<MainScrollContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainScrollContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainScrollContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
