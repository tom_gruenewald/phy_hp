import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourdateVideoComponent } from './tourdate-video.component';

describe('TourdateVideoComponent', () => {
  let component: TourdateVideoComponent;
  let fixture: ComponentFixture<TourdateVideoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourdateVideoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourdateVideoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
