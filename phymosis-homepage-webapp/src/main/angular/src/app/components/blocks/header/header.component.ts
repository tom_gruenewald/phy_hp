import { Component} from '@angular/core';
import {ReplaySubject} from "rxjs";
import {distinctUntilChanged} from "rxjs/operators";
import {UserService} from "../../../services/user-service/user.service";

@Component({
  selector: 'phy-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  public navigationActive: boolean = false;

  private readonly visibleIconStyle = "material-icons header-menu-icon";

  private $menuIconStyleSubject = new ReplaySubject<string>(1);
  public $menuIconStyle = this.$menuIconStyleSubject.asObservable().pipe(distinctUntilChanged());

  private $closeIconStyleSubject = new ReplaySubject<string>(1);
  public $closeIconStyle = this.$closeIconStyleSubject.asObservable().pipe(distinctUntilChanged());

  constructor(public readonly userService: UserService) {
    this.$menuIconStyleSubject.next(this.visibleIconStyle);
    this.$closeIconStyleSubject.next(this.visibleIconStyle + ' icon-closed');
  }

  public closeNavigation() {
    this.navigationActive = false;
    this.$menuIconStyleSubject.next(this.visibleIconStyle);
    this.$closeIconStyleSubject.next(this.visibleIconStyle + ' icon-closed');
  }

  public openNavigation() {
    this.navigationActive = true;
    this.$menuIconStyleSubject.next(this.visibleIconStyle + ' icon-closed');
    this.$closeIconStyleSubject.next(this.visibleIconStyle);
  }

  public setNavigationActive(navigationActive:boolean) {
    this.navigationActive = navigationActive;
    if (navigationActive) {
      this.$menuIconStyleSubject.next(this.visibleIconStyle + ' icon-closed');
      this.$closeIconStyleSubject.next(this.visibleIconStyle);
    } else {
      this.$menuIconStyleSubject.next(this.visibleIconStyle);
      this.$closeIconStyleSubject.next(this.visibleIconStyle + ' icon-closed');
    }
  }

}
