import { Component } from '@angular/core';

@Component({
  selector: 'phy-impressum-footer',
  templateUrl: './impressum-footer.component.html',
  styleUrls: ['./impressum-footer.component.scss']
})
export class ImpressumFooterComponent {

  public impressum: boolean;
  public form: boolean;

  constructor() {
    this.impressum = true;
    this.form = false;
  }

  public toggleImpressum() {
    this.impressum = !this.impressum;
  }

  public toggleForm() {
    this.form = !this.form;
  }

}
