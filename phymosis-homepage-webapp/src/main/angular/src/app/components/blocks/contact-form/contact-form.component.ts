import {Component, OnDestroy} from '@angular/core';
import {MailService} from "../../../services/mail-service/mail.service";
import {ReplaySubject, Subscription} from "rxjs";
import {distinctUntilChanged, shareReplay} from "rxjs/operators";
import {MailErrorType} from "../../../beans/enums/mail-error-type";


export class ContactFormModel {
  public name: string;
  public subject: string;
  public email: string;
  public message: string;
}

@Component({
  selector: 'phy-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnDestroy {

  public readonly MailErrorType = MailErrorType;

  private readonly $MailStatusSubject = new ReplaySubject<MailErrorType | null>(1);
  public readonly $MailStatus = this.$MailStatusSubject
    .asObservable()
    .pipe(
      distinctUntilChanged()
    );

  private readonly currentEmail = "phymosis@web.de";

  public model = new ContactFormModel();

  private readonly $submittedSubject = new ReplaySubject<boolean>(1);
  public readonly $submitted = this.$submittedSubject
    .asObservable()
    .pipe(
      distinctUntilChanged(),
      shareReplay(),
    );
  private submittedSubscription: Subscription;

  constructor(private readonly mailService: MailService) {
    this.$submittedSubject.next(false);
    this.$MailStatusSubject.next(null);
    this.submittedSubscription = this.$submitted.subscribe(
      submitted => {
        if (submitted)
          this.sendEmail();
      });
  }

  ngOnDestroy() {
    this.submittedSubscription.unsubscribe();
  }

  private sendEmail() {
    if (this.checkMailAddressIntegrity(this.model.email)) {
      this.$MailStatusSubject.next(null);
      const title = 'Homepage Request: ' + this.model.subject
        + ' send by ' + this.model.name
        + ' | ' + this.model.email;

      const text = this.model.subject
        + '\nSender: ' + this.model.name
        + '\nE-Mail address: ' + this.model.email
        + '\nDate: ' + new Date().toDateString()
        + '\n\n\t' + this.model.message
        + '\n\n...answer to ' + this.model.email;

      this.mailService.sendMail$(this.currentEmail, text, title)
        .toPromise()
        .then(mail => {
          if (mail.from === "error") {
            switch (mail.title) {
              case MailErrorType.SEND:
                this.$MailStatusSubject.next(MailErrorType.SEND);
                break;
              case MailErrorType.AUTHENTICATION:
                this.$MailStatusSubject.next(MailErrorType.AUTHENTICATION);
                break;
              case MailErrorType.PARSE:
                this.$MailStatusSubject.next(MailErrorType.PARSE);
                break;
              default:
                this.$MailStatusSubject.next(MailErrorType.AUTHENTICATION);
            }
          } else {
            this.$MailStatusSubject.next(MailErrorType.NONE);
          }
        })
        .catch(()=>this.$MailStatusSubject.next(MailErrorType.NOT_AVAILABLE));
    } else
      this.$MailStatusSubject.next(MailErrorType.SEND);
  }

  public onSubmit() {
    this.$submittedSubject.next(true);
  }

  public onClick() {
    this.model = new ContactFormModel();
    this.$submittedSubject.next(false);
  }

  private checkMailAddressIntegrity(mailAddress: string): boolean {
    let atIdx = mailAddress.indexOf('@');
    return (atIdx > 0) && (mailAddress.indexOf('.', atIdx) > atIdx + 1);
  }
}
