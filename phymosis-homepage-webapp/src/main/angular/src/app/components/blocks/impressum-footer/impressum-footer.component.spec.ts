import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImpressumFooterComponent } from './impressum-footer.component';

describe('ImpressumFooterComponent', () => {
  let component: ImpressumFooterComponent;
  let fixture: ComponentFixture<ImpressumFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImpressumFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImpressumFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
