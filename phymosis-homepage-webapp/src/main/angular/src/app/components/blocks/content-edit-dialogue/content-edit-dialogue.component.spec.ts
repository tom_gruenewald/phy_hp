import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentEditDialogueComponent } from './content-edit-dialogue.component';

describe('ContentEditDialogueComponent', () => {
  let component: ContentEditDialogueComponent;
  let fixture: ComponentFixture<ContentEditDialogueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContentEditDialogueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentEditDialogueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
