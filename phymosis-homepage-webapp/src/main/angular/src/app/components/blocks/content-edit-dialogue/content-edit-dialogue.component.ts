import {Component, Input, OnInit} from '@angular/core';
import {ContentType} from '../../../beans/enums/content-type';
import {MediaType} from "../../../beans/enums/media-type";
import {ContentBean} from "../../../beans/content-bean";
import {MediaLinkBean} from "../../../beans/media-link-bean";
import {SocialMediaLinkBean} from "../../../beans/social-media-link-bean";
import {SocialMediaSite} from "../../../beans/enums/social-media-site";
import {TourdateBean} from "../../../beans/tourdate-bean";
import {convertYouTubeLink} from "../../../utils/basic-utils";


/*
TODO: Make it sanitize the youtube and drive links to usable ones for the iframe / image
 e.g. Drive needs:

https://drive.google.com/file/d/1M-smhXJUL0br8tkWpQS1FzL4SnjGAlq3/view?usp=sharing
TODO: converted into
https://drive.google.com/uc?id=1M-smhXJUL0br8tkWpQS1FzL4SnjGAlq3

TODO: While YT wants something like:

https://www.youtube.com/watch?v=Rj01aT_Ihhg&list=RDMMRj01aT_Ihhg&index=1
TODO: converted into
https://www.youtube.com/embed/Rj01aT_Ihhg

TODO: NOTE: IN BOTH CASES THE IDS NEEDED ARE NESTED IN THE NORMAL YT-LINK EMBED AND GOOGLE DRIVE SHARING LINK
 */
export class ContentFormModel {
  public contentType: ContentType;
  public title: string;
  public text: string;
  public mediaType?: MediaType;
  public mediaLink?: string;
  public socialMediaSite?: SocialMediaSite;
  public socialMediaLink?: string;
  public googleMapsLink?: string;
  public date?: Date;
  public location?: string;
}

@Component({
  selector: 'phy-content-edit-dialogue',
  templateUrl: './content-edit-dialogue.component.html',
  styleUrls: ['./content-edit-dialogue.component.scss']
})
export class ContentEditDialogueComponent implements OnInit {

  public readonly SocialMediaSite = SocialMediaSite;
  public readonly ContentType = ContentType;
  public readonly MediaType = MediaType;

  public model: ContentFormModel;

  public readonly availableTypes: ContentType[] = [
    ContentType.NEWS,
    ContentType.SONG,
    ContentType.RELEASE,
    ContentType.TOURDATE,
  ];

  /**
   * If set, the dialogue opens up in edit mode for the specific content
   */
  @Input()
  public targetContent: ContentBean = null;
  @Input()
  public targetTourdate: TourdateBean = null;

  // todo change default to false
  @Input()
  public showEditDialogue: boolean = true;

  @Input()
  public targetMediaLink: MediaLinkBean = null;

  @Input()
  public targetSocialMediaLink: SocialMediaLinkBean = null;

  @Input()
  public contentType: ContentType = null;

  constructor() {
    this.model = new ContentFormModel();
  }

  ngOnInit() {
    if (this.targetContent !== null) {
      this.model.title = this.targetContent.title;
      this.model.text = this.targetContent.text;
      this.model.mediaType = this.targetContent.mediaType;
      this.model.contentType = this.targetContent.contentType;
      if (this.targetMediaLink !== null) {
        this.model.mediaLink = this.targetMediaLink.mediaLink;
      }
      if (this.targetSocialMediaLink !== null) {
        this.model.socialMediaSite = this.targetSocialMediaLink.socialMediaSite;
        this.model.socialMediaLink = this.targetSocialMediaLink.mediaLink;
      }
    } else if(this.targetTourdate !== null) {
      this.model.title = this.targetTourdate.title;
      this.model.text = this.targetTourdate.text;
      this.model.location = this.targetTourdate.location;
      this.model.googleMapsLink = this.targetTourdate.googleMapsLink;
      this.model.date = this.targetTourdate.date;
    } else {
      if (this.contentType !== null) {
        this.model.contentType = this.contentType;
      } else {
        this.model.contentType = ContentType.NEWS;
      }
      this.model.socialMediaLink = null;
      this.model.mediaType = MediaType.NONE;
      this.model.mediaLink = null;
      this.model.title = "Enter title here...";
      this.model.text = "Enter text here...";
      this.model.socialMediaSite = SocialMediaSite.BANDCAMP;
    }
  }

  public toggleEditDialogue(): void {
    this.showEditDialogue = !this.showEditDialogue;
  }

  public getMediaLink(): string {
    switch (this.model.mediaType) {
      case MediaType.YOUTUBE_VIDEO:
        return convertYouTubeLink(this.model.mediaLink);
      case MediaType.BANDCAMP_LINK:
        break;
      case MediaType.IMAGE:
        break;
      default:
        return null;
    }
  }
}
