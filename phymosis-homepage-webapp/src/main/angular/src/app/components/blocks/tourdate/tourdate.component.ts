import {Component, Input} from '@angular/core';
import {TourdateBean} from "../../../beans/tourdate-bean";

@Component({
  selector: 'phy-tourdate',
  templateUrl: './tourdate.component.html',
  styleUrls: ['./tourdate.component.scss']
})
export class TourdateComponent {

  @Input()
  public tourdateBean: TourdateBean;

  public hidableContainerStyle = "live-page-tourdate-hidable-container details-hidden";

  constructor() {
  }

  public toggleDetailsVisibility(): void {
        this.hidableContainerStyle === "live-page-tourdate-hidable-container details-shown"
          ? this.hidableContainerStyle += "live-page-tourdate-hidable-container details-hidden"
          : this.hidableContainerStyle = "live-page-tourdate-hidable-container details-shown";
  }

}
