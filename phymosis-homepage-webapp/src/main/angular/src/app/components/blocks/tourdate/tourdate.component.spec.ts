import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TourdateComponent } from './tourdate.component';

describe('TourdateComponent', () => {
  let component: TourdateComponent;
  let fixture: ComponentFixture<TourdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TourdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TourdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
