import {AfterContentInit, Component, Input} from '@angular/core';
import {TourdateBean} from "../../../beans/tourdate-bean";
import {MediaLinkService} from "../../../services/media-link-service/media-link.service";

@Component({
  selector: 'phy-tourdate-video',
  templateUrl: './tourdate-video.component.html',
  styleUrls: ['./tourdate-video.component.scss']
})
export class TourdateVideoComponent implements AfterContentInit {

  @Input()
  public tourdateBean: TourdateBean;

  public tourVideoURL: string | undefined = undefined;

  constructor(private readonly mediaLinkService: MediaLinkService) {
  }

  ngAfterContentInit(): void {
    this.mediaLinkService.getMediaByContentId(this.tourdateBean.id)
      .toPromise()
      .then(mediaLinkBean => this.tourVideoURL = mediaLinkBean[0].mediaLink);
  }

}
