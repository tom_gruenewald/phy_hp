import {Component, OnDestroy} from '@angular/core';
import {Observable, ReplaySubject, Subscription} from "rxjs";
import {distinctUntilChanged, share, shareReplay} from "rxjs/operators";
import {UserService} from "../../../services/user-service/user.service";
import {UserBean} from "../../../beans/user-bean";

export class LoginFormModel {
  public name: string;
  public password: string;
}

@Component({
  selector: 'phy-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnDestroy {

  public readonly model = new LoginFormModel();

  public readonly $user: Observable<UserBean>;

  private readonly $submittedSubject = new ReplaySubject<boolean>(1);
  public readonly $submitted = this.$submittedSubject
    .asObservable()
    .pipe(
      distinctUntilChanged(),
      share(),
    );
  private submitSubscription: Subscription;

  constructor(private readonly userService: UserService) {
    this.$submittedSubject.next(false);
    this.submitSubscription = this.$submitted.subscribe(submitted => {
      if (submitted)
        this.userService.login(this.model.name, this.model.password)
      }
    );
    this.$user = userService.$user;
  }

  public onSubmit() {
    this.$submittedSubject.next(true);
  }

  ngOnDestroy(): void {
    this.submitSubscription.unsubscribe();
  }
}
