import {Component, EventEmitter, HostListener, Output} from '@angular/core';
import {RouteScrollService} from "../../../../routing/route-scroll.service";
import {RouteTemplate} from "../../../../routing/route-template";

@Component({
  selector: 'phy-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent {

  private readonly smallBreakpointMax = 799;
  private scrWidth: number;

  @HostListener('window:resize', ['$event'])
  getScreenSize(event?) {
    this.scrWidth = window.innerWidth;
  }

  @Output()
  public navigationActive = new EventEmitter<boolean>();

  public readonly RouteTemplate = RouteTemplate;

  constructor(private readonly routeScrollService: RouteScrollService) {
    this.scrWidth = window.innerWidth;
  }

  public navigateTo(routeTemplate: RouteTemplate): void {
    this.routeScrollService.navigateTo(routeTemplate);
    this.closeNavigationOnSmallScreens();
  }

  public closeNavigationOnSmallScreens() {
    if (this.scrWidth <= this.smallBreakpointMax)
      this.closeNavigation();
  }

  public closeNavigation() {
    this.navigationActive.emit(false);
  }

}
