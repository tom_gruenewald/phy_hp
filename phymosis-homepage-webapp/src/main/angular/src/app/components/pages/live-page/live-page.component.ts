import {Component, OnDestroy, OnInit} from '@angular/core';
import {ReplaySubject, Subscription} from "rxjs";
import {TourdateBean} from "../../../beans/tourdate-bean";
import {ContentBean} from "../../../beans/content-bean";
import {TourdateService} from "../../../services/tourdate-service/tourdate.service";
import {ContentService} from "../../../services/content-service/content.service";
import {distinctUntilChanged} from "rxjs/operators";

@Component({
  selector: 'phy-live-page',
  templateUrl: './live-page.component.html',
  styleUrls: ['./live-page.component.scss']
})
export class LivePageComponent implements OnDestroy {

  private readonly $tourDatePagesShownSubject = new ReplaySubject<number>(1);
  public readonly tourDatePagesShownSubscription: Subscription;

  private readonly $tourDatesSubject = new ReplaySubject<TourdateBean[]>(1);
  public readonly $tourDates = this.$tourDatesSubject.asObservable();

  public readonly pastTourDates: TourdateBean[] = [];

  constructor(private readonly tourdateService: TourdateService,
              private readonly contentService: ContentService) {
    tourdateService.$getUpcomingTourDatesPageable(0,10)
      .toPromise()
      .then(tourDates => this.$tourDatesSubject.next(tourDates));
    this.tourDatePagesShownSubscription = this.$tourDatePagesShownSubject
      .asObservable()
      .pipe(
        distinctUntilChanged()
      )
      .subscribe(
        page => this.tourdateService.$getPastTourDatesPageable(page, 2)
          .toPromise()
          .then(
            tourdates => tourdates
              .map(
                tourdate => this.pastTourDates.push(tourdate)
              )
          )
      );
    this.$tourDatePagesShownSubject.next(0);
  }

  ngOnDestroy() {
    this.tourDatePagesShownSubscription.unsubscribe();
  }

}
