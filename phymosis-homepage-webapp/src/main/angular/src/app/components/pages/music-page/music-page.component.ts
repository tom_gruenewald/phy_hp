import {Component, OnDestroy} from '@angular/core';
import {ContentService} from "../../../services/content-service/content.service";
import {combineLatest, ReplaySubject, Subscription} from "rxjs";
import {MediaLinkService} from "../../../services/media-link-service/media-link.service";
import {ContentType} from "../../../beans/enums/content-type";
import {ContentOrderType} from "../../../beans/enums/content-order-type";
import {MediaLinkBean} from "../../../beans/media-link-bean";
import {ContentBean} from "../../../beans/content-bean";

@Component({
  selector: 'phy-music-page',
  templateUrl: './music-page.component.html',
  styleUrls: ['./music-page.component.scss']
})
export class MusicPageComponent implements OnDestroy {

  public readonly ContentType = ContentType;

  private songPageNumber = 0;
  private releasePageNumber = 0;

  private songsAvailable: number;
  private releasesAvailable: number;

  public releasesButtonStyle = "music-type-selector selected";
  public songsButtonStyle = "music-type-selector";

  private $currentContentTypeSubject = new ReplaySubject<ContentType>(1);
  public $currentContentType = this.$currentContentTypeSubject.asObservable();

  private contentSubscription: Subscription;

  private $fetchedReleaseLinksSubject = new ReplaySubject<MediaLinkBean[]>(1);
  public $fetchedReleaseLinks = this.$fetchedReleaseLinksSubject.asObservable();
  private fetchedReleaseLinksSubscription: Subscription;

  private $fetchedSongLinksSubject = new ReplaySubject<MediaLinkBean[]>(1);
  public $fetchedSongLinks = this.$fetchedSongLinksSubject.asObservable();
  private fetchedSongLinksSubscription: Subscription;

  public readonly releaseMediaLinks: MediaLinkBean[] = [];
  public readonly songMediaLinks: MediaLinkBean[] = [];
  private readonly contentBeans: ContentBean[] = [];


  constructor(private readonly contentService: ContentService,
              public readonly mediaLinkService: MediaLinkService) {
    this.contentSubscription = combineLatest([
      this.$currentContentType
    ]).subscribe(([contentType]) => {
      let page: number;
      let size: number;
      if (contentType === ContentType.RELEASE) {
        page = this.releasePageNumber;
        size = 2;
      } else {
        page = this.songPageNumber;
        size = 6;
      }
      this.contentService
        .$getContentByTypeAndPage(contentType, page, size, ContentOrderType.DATE_DESC)
        .toPromise()
        .then(
          content => content.map(content => {
              MusicPageComponent.pushContentIfNotPresent(this.contentBeans, content);
              this.mediaLinkService.getMediaByContentId(content.id)
                .toPromise()
                .then(mediaLinkBeans => {
                  switch (contentType) {
                    case ContentType.RELEASE:
                      this.$fetchedReleaseLinksSubject.next(mediaLinkBeans);
                      break;
                    case ContentType.SONG:
                      this.$fetchedSongLinksSubject.next(mediaLinkBeans);
                  }
                })
            }
          ));
    });
    this.fetchedSongLinksSubscription = this.$fetchedSongLinks.subscribe(
      mediaLinkBeans => MusicPageComponent.pushIfNotPresent(this.songMediaLinks, mediaLinkBeans)
    );
    this.fetchedReleaseLinksSubscription = this.$fetchedReleaseLinks.subscribe(
      mediaLinkBeans => MusicPageComponent.pushIfNotPresent(this.releaseMediaLinks, mediaLinkBeans)
    );
    this.$currentContentTypeSubject.next(ContentType.RELEASE);
    this.contentService.$getActiveContentNumber(ContentType.RELEASE).toPromise().then(number => this.releasesAvailable = number);
    this.contentService.$getActiveContentNumber(ContentType.SONG).toPromise().then(number => this.songsAvailable = number);
  }

  public goToReleases() {
    this.$currentContentType.subscribe(contentType => {
      if (contentType === ContentType.SONG) {
        this.$currentContentTypeSubject.next(ContentType.RELEASE);
        this.releasesButtonStyle = "music-type-selector selected";
        this.songsButtonStyle = "music-type-selector";
      }
    }).unsubscribe();
  }

  public goToSongs() {
    this.$currentContentType.subscribe(contentType => {
      if (contentType === ContentType.RELEASE) {
        this.$currentContentTypeSubject.next(ContentType.SONG);
        this.releasesButtonStyle = "music-type-selector";
        this.songsButtonStyle = "music-type-selector selected";
      }
    }).unsubscribe();
  }

  ngOnDestroy(): void {
    this.contentSubscription.unsubscribe();
    this.fetchedReleaseLinksSubscription.unsubscribe();
    this.fetchedSongLinksSubscription.unsubscribe();
  }

  private static pushContentIfNotPresent(pushTo: Array<ContentBean>, item: ContentBean): void {
    if (pushTo.length > 0) {
      let ContentIds = new Array<number>();
      for (const ContentBean of pushTo) {
        ContentIds.push(ContentBean.id)
      }
      if (!ContentIds.includes(item.id))
        pushTo.push(item);
    } else {
      pushTo.push(item);
    }
  }

  private static pushIfNotPresent(pushTo: Array<MediaLinkBean>, items: MediaLinkBean[]): void {
    if (pushTo.length > 0) {
      let ContentIds = new Array<number>();
      for (const mediaLinkBean of pushTo) {
        ContentIds.push(mediaLinkBean.contentId)
      }
      for (const item of items) {
        if (!ContentIds.includes(item.contentId))
          pushTo.push(item);
      }
    } else {
      for (const item of items) {
        pushTo.push(item);
      }
    }
  }

  public loadMoreReleases() {
    this.releasePageNumber++;
    this.$currentContentTypeSubject.next(ContentType.RELEASE);
  }

  public loadMoreSongs() {
    this.songPageNumber++;
    this.$currentContentTypeSubject.next(ContentType.SONG);
  }

  public getContentBeansByType(contentType: ContentType): ContentBean[] {
    return this.contentBeans
      .filter(content => content.contentType === contentType)
      .sort((a, b) => (new Date(b.creationDate)).getTime() - (new Date(a.creationDate)).getTime())
  }

  public getReleaseLinkByContentId(contentId: number): MediaLinkBean {
    for (const releaseMediaLink of this.releaseMediaLinks) {
      if (releaseMediaLink.contentId === contentId)
        return releaseMediaLink;
    }
  }

  public getSongLinkByContentId(contentId: number): MediaLinkBean {
    for (const releaseMediaLink of this.songMediaLinks) {
      if (releaseMediaLink.contentId === contentId)
        return releaseMediaLink;
    }
  }

}
