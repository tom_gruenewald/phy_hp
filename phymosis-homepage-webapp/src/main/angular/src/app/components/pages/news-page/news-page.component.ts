import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnChanges,
  OnDestroy,
  SimpleChanges
} from '@angular/core';
import {ContentService} from "../../../services/content-service/content.service";
import {combineLatest, Observable, ReplaySubject, Subscription} from "rxjs";
import {ContentBean} from "../../../beans/content-bean";
import {ContentType} from "../../../beans/enums/content-type";
import {ContentOrderType} from "../../../beans/enums/content-order-type";
import {distinctUntilChanged, map, switchMap, tap} from "rxjs/operators";
import {MediaLinkService} from "../../../services/media-link-service/media-link.service";
import {MediaLinkBean} from "../../../beans/media-link-bean";
import {MediaType} from "../../../beans/enums/media-type";
import {SocialMediaLinkBean} from "../../../beans/social-media-link-bean";
import {SocialLinkService} from "../../../services/social-link-service/social-link.service";
import {SocialMediaSite} from "../../../beans/enums/social-media-site";
import {UserService} from "../../../services/user-service/user.service";
import {UserBean} from "../../../beans/user-bean";

@Component({
  selector: 'phy-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.scss'],
})
export class NewsPageComponent implements OnDestroy {

  private readonly $showInactiveSubject = new ReplaySubject<boolean>(1);
  public readonly $showInactive = this.$showInactiveSubject
    .asObservable()
    .pipe(
      tap(show => show
        ? this.contentService.$getContentNumber(ContentType.NEWS)
          .toPromise()
          .then(number => this.newsNumber = number)
          .catch(() => this.newsNumber = 8)
        : this.contentService.$getActiveContentNumber(ContentType.NEWS)
          .toPromise()
          .then(number => this.newsNumber = number)
          .catch(() => this.newsNumber = 8)
      )
    );

  public readonly MediaType = MediaType;
  public readonly SocialMediaSite = SocialMediaSite;

  private $currentPageSubject = new ReplaySubject<number>(1);
  public $currentPage = this.$currentPageSubject.asObservable();

  private $currentSizeSubject = new ReplaySubject<number>(1);
  public $currentSize = this.$currentSizeSubject.asObservable();

  private $currentMediaLinksSubject = new ReplaySubject<MediaLinkBean[]>(1);
  public $currentMediaLinks = this.$currentMediaLinksSubject.asObservable();
  private mediaLinksSubscription: Subscription;

  private $currentSocialMediaLinksSubject = new ReplaySubject<SocialMediaLinkBean[]>(1);
  public $currentSocialMediaLinks = this.$currentSocialMediaLinksSubject.asObservable();

  private newsContainerNumber: number;

  private newsNumber: number;

  public $currentContent: Observable<ContentBean[]>;

  constructor(private readonly contentService: ContentService,
              private readonly mediaLinkService: MediaLinkService,
              private readonly socialMediaLinkService: SocialLinkService,
              public readonly userService: UserService,
              private readonly checkRef: ChangeDetectorRef,) {
    this.contentService.$getActiveContentNumber(ContentType.NEWS)
      .toPromise()
      .then(number => this.newsNumber = number)
      .catch(() => this.newsNumber = 8);
    this.resetPage();
    this.$currentSizeSubject.next(4);
    this.$showInactiveSubject.next(false);

    this.$currentContent = combineLatest([
      this.$currentPage,
      this.$currentSize,
      this.$showInactive,
    ]).pipe(
      switchMap(([page, size, showInactive]) => {
        let contentBeans = this.contentService.$getContentByTypeAndPage(
          ContentType.NEWS,
          page,
          size,
          ContentOrderType.DATE_DESC);
        return showInactive
          ? contentBeans
          : contentBeans.pipe(
            map(contentBeans => contentBeans
              .filter(contentBean => contentBean.active === true),
            )
          );
      }),
      distinctUntilChanged(),
      tap(() => this.newsContainerNumber = 1),
    );
    this.mediaLinksSubscription = this.$currentContent.pipe(
      map(contentBeans => contentBeans
        .filter(contentBean => contentBean.mediaType !== MediaType.NONE)
      ),
      map(contentBeans => {
        let socialLinks = new Array<SocialMediaLinkBean>();
        let mediaLinks = new Array<MediaLinkBean>();
        for (const contentBean of contentBeans) {
          this.mediaLinkService.getMediaByContentId(contentBean.id).toPromise().then(
            mediaLinkBeans => mediaLinkBeans.map(mediaLink => mediaLinks.push(mediaLink))
          );
          this.socialMediaLinkService.getMediaByContentId(contentBean.id).toPromise().then(
            socialLink => socialLinks.push(socialLink)
          );
        }
        this.$currentSocialMediaLinksSubject.next(socialLinks);
        this.$currentMediaLinksSubject.next(mediaLinks);
      })
    ).subscribe();
  }


  public getContentMedia(contentId: number): Observable<MediaLinkBean> {
    return this.$currentMediaLinks.pipe(
      map<MediaLinkBean[], MediaLinkBean>(mediaLinks =>
        mediaLinks.find(mediaLink => mediaLink.contentId === contentId)),
    );
  }

  public getSocialMediaLink(contentId: number): Observable<SocialMediaLinkBean> {
    return this.$currentSocialMediaLinks.pipe(
      map<SocialMediaLinkBean[], SocialMediaLinkBean>(socialLinks =>
        socialLinks.find(sLink => sLink.contentId === contentId)),
    );
  }

  getContainerStyles() {
    if (this.newsContainerNumber % 2 == 1) {
      this.newsContainerNumber = 2;
      return "news-container left-news-container"
    }
    this.newsContainerNumber = 1;
    return "news-container right-news-container"
  }

  loadMoreNews() {
    let newCount;
    this.$currentSize.subscribe(count => newCount = count + 4).unsubscribe();
    this.$currentSizeSubject.next(newCount);
  }

  ngOnDestroy(): void {
    this.mediaLinksSubscription.unsubscribe();
  }

  deleteNews(newsId: number, user: UserBean) {
    this.contentService.$deactivateContentById(newsId, user.tokenBean).toPromise();
    this.contentService.$getContentNumber(ContentType.NEWS)
      .toPromise()
      .then(number => this.newsNumber = number)
      .catch(() => this.newsNumber = 8);
    this.resetPage();
  }

  private resetPage() {
    this.$currentMediaLinksSubject.next(null);
    this.$currentPageSubject.next(0);
  }

  reactivateNews(newsId: number, user: UserBean) {
    this.contentService.$activateContentById(newsId, user.tokenBean).toPromise();
    this.contentService.$getContentNumber(ContentType.NEWS)
      .toPromise()
      .then(number => this.newsNumber = number)
      .catch(() => this.newsNumber = 8);
    this.resetPage();
  }

  showDeactivatedNews() {
    let showInactive = false;
    this.$showInactive.subscribe(show => showInactive = show).unsubscribe();
    this.$showInactiveSubject.next(!showInactive);
  }
}
