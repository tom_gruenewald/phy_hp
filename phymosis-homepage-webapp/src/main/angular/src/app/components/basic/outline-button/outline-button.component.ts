import {Component, Input} from '@angular/core';

@Component({
  selector: 'phy-outline-button',
  templateUrl: './outline-button.component.html',
  styleUrls: ['./outline-button.component.scss']
})
export class OutlineButtonComponent {

  @Input('text')
  public text ='text';

  constructor() { }


}
