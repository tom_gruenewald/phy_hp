import { Injectable } from '@angular/core';
import {HttpService} from '../http-service/http.service';
import {environment} from '../../../environments/environment';
import {EMPTY, Observable} from 'rxjs';
import {isSafe} from "../../utils/safety-utils";
import {concatUrl, KeyValueStore} from "../../utils/basic-utils";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private readonly httpService: HttpService) { }


  public get$<T>(
    restUrl: string,
    parameters?: KeyValueStore<string>,
  ): Observable<T> {
    if (!isSafe(restUrl)) {
      return EMPTY;
    }
    return this.httpService.get$(concatUrl(environment.restBaseUrl, environment.serverRestPrefix, restUrl), parameters);
  }


  public post$<T>(
    restUrl: string,
    payload: any,
    parameters?: KeyValueStore<string>,
  ): Observable<T> {
    if (!isSafe(restUrl)) {
      return EMPTY;
    }
    return this.httpService.post$(concatUrl(environment.restBaseUrl, environment.serverRestPrefix, restUrl), payload, parameters);
  }


  public delete$<T>(
    restUrl: string,
    parameters?: KeyValueStore<string>,
  ): Observable<T> {
    if (!isSafe(restUrl)) {
      return EMPTY;
    }
    return this.httpService.delete$(concatUrl(environment.restBaseUrl, environment.serverRestPrefix, restUrl), parameters);
  }

}
