import {Injectable} from '@angular/core';
import {ReplaySubject} from "rxjs";
import {UserBean} from "../../beans/user-bean";
import {RestService} from "../rest-service/rest.service";
import {share, shareReplay, tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private readonly userControllerURL = "/user";
  private readonly authControllerURL = "/auth";

  private $userSubject = new ReplaySubject<UserBean | null>(1);
  public $user = this.$userSubject
    .asObservable();

  constructor(private readonly restService: RestService) {
    this.$userSubject.next(null);
  }


  public login(userName: string, password: string) {
    this.restService.get$<UserBean>(this.authControllerURL + '/login', {
      name: userName,
      password: password,
    }).pipe(
      share(),
    )
      .toPromise()
      .then(
        user => this.$userSubject.next(user)
      )
      .catch(() => this.$userSubject.next(null));
  }

  public logout(userId: number) {
    this.$userSubject.next(null);
    this.restService.get$(this.authControllerURL + '/logout', {
        userId: userId.toString()
      }).toPromise();
  }

}
