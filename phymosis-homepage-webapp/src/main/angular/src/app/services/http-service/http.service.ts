import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {map, shareReplay} from 'rxjs/operators';
import {KeyValueStore} from "../../utils/basic-utils";
import {isSafe} from "../../utils/safety-utils";

@Injectable()
export class HttpService {

  constructor(private readonly httpClient: HttpClient) {
  }

  public get$<T>(url: string, parameters?: KeyValueStore<string>): Observable<T | null> {
    return this.executeHttpRequest$((options: any) => {
      return this.httpClient.get<T>(url, options) as Observable<HttpResponse<T>>;
    }, parameters);
  }

  public post$<T>(url: string, body: any, parameters?: KeyValueStore<string>): Observable<T | null> {
    return this.executeHttpRequest$((options: any) => {
      return this.httpClient.post<T>(url, body, options) as Observable<HttpResponse<T>>;
    }, parameters);
  }

  public delete$<T>(url: string, parameters?: KeyValueStore<string>): Observable<T | null> {
    return this.executeHttpRequest$((options: any) => {
      return this.httpClient.delete<T>(url, options) as Observable<HttpResponse<T>>;
    }, parameters);
  }

  private executeHttpRequest$<T>(execute: (options: any) => Observable<HttpResponse<T>>, parameters?: KeyValueStore<string>): Observable<T | null> {
    const options: any = {
      headers: new HttpHeaders(),
      withCredentials: true,
      observe: 'response',
      params: (isSafe(parameters) ? parameters : {}),
    };
    return execute(options).pipe(
      map(it => it.body),
      shareReplay(1),
    );
  }
}
