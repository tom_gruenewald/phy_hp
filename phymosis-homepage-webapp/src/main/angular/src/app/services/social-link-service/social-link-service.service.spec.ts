import { TestBed } from '@angular/core/testing';

import { SocialLinkService } from './social-link.service';

describe('SocialLinkServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SocialLinkService = TestBed.get(SocialLinkService);
    expect(service).toBeTruthy();
  });
});
