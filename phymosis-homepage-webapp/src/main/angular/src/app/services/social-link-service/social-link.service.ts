import { Injectable } from '@angular/core';
import {RestService} from "../rest-service/rest.service";
import {Observable} from "rxjs";
import {SocialMediaLinkBean} from "../../beans/social-media-link-bean";

@Injectable({
  providedIn: 'root'
})
export class SocialLinkService {

  private contentURL = '/content';

  constructor(private readonly restService: RestService) { }


  public getMediaByContentId(contentId: number): Observable<SocialMediaLinkBean> {
    return this.restService.get$(this.contentURL + '/getSocialLinkByContentId', {
      contentId: contentId.valueOf().toString()
    });
  }
}
