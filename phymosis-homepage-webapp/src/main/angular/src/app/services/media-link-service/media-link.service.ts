import { Injectable } from '@angular/core';
import {RestService} from "../rest-service/rest.service";
import {Observable} from "rxjs";
import {MediaLinkBean} from "../../beans/media-link-bean";

@Injectable({
  providedIn: 'root'
})
export class MediaLinkService {

  private mediaURL = '/content/media';

  constructor(private readonly restService: RestService) {

  }

  public getMediaByContentId(contentId: number): Observable<MediaLinkBean[]> {
    return this.restService.get$(this.mediaURL + '/getByContentId', {
      contentId: contentId.valueOf().toString()
    });
  }
}
