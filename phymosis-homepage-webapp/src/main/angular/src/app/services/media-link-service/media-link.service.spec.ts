import { TestBed } from '@angular/core/testing';

import { MediaLinkService } from './media-link.service';

describe('MediaLinkService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MediaLinkService = TestBed.get(MediaLinkService);
    expect(service).toBeTruthy();
  });
});
