import {Injectable} from '@angular/core';
import {RestService} from "../rest-service/rest.service";
import {ContentType} from "../../beans/enums/content-type";
import {Observable} from "rxjs";
import {ContentBean} from "../../beans/content-bean";
import {ContentOrderType} from "../../beans/enums/content-order-type";
import {TokenBean} from "../../beans/token-bean";
import {stringifyBoolean} from "../../utils/basic-utils";

@Injectable({
  providedIn: 'root'
})
export class ContentService {

  private readonly contentURL = '/content';

  constructor(private readonly restService: RestService) {
  }

  public $getContentByTypeAndPage(type: ContentType, page: number, size: number, orderType: ContentOrderType, active: boolean = true): Observable<ContentBean[]> {
    return this.restService.get$(this.contentURL + '/getPageByType',
      {
        type: type,
        page: page.valueOf().toString(),
        size: size.valueOf().toString(),
        orderType: orderType,
        active: stringifyBoolean(active),
      }
    )
  }

  public $getActiveContentNumber(type: ContentType): Observable<number> {
    return this.restService.get$(this.contentURL + '/numberActive',
      {
        type: type
      }
    )
  }

  public $getContentNumber(type: ContentType): Observable<number> {
    return this.restService.get$(this.contentURL + '/number',
      {
        type: type
      }
    )
  }

  public $deactivateContentById(contentId: number, token: TokenBean): Observable<number> {
    return this.restService.get$(this.contentURL + '/deactivate',
      {
        contentId: contentId.toString(),
        token: TokenBean.stringify(token),
      }
    )
  }

  public $activateContentById(contentId: number, token: TokenBean): Observable<number> {
    return this.restService.get$(this.contentURL + '/activate',
      {
        contentId: contentId.toString(),
        token: TokenBean.stringify(token),
      }
    )
  }

}
