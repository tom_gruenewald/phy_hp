import {Injectable} from '@angular/core';
import {RestService} from "../rest-service/rest.service";
import {Observable} from "rxjs";
import {TourdateBean} from "../../beans/tourdate-bean";

@Injectable({
  providedIn: 'root'
})
export class TourdateService {

  private readonly tourdateURL = 'tourDates';

  constructor(private readonly restService: RestService) {
  }

  public $getActiveTourdates(): Observable<TourdateBean[]> {
    return this.restService.get$(this.tourdateURL + '/getTourDates');
  }

  public $getPastTourDatesPageable(page: number, size: number): Observable<TourdateBean[]> {
    return this.restService
      .get$(this.tourdateURL + '/getPastTourDatesPage',{
        page: page.toString(),
        size: size.toString(),
      });
  }

  public $getUpcomingTourDatesPageable(page: number, size: number): Observable<TourdateBean[]> {
    return this.restService
      .get$(this.tourdateURL + '/getUpcomingTourDatesPage',{
        page: page.toString(),
        size: size.toString(),
      });
  }
}
