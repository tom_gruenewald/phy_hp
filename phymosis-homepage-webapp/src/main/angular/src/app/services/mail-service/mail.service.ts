import {Injectable} from '@angular/core';
import {RestService} from "../rest-service/rest.service";
import {MailBean} from "../../beans/mail-bean";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class MailService {

  private mailURL = '/mail';

  constructor(private readonly restService: RestService) {
  }

  public sendMail$(to: string,
                   text: string,
                   title?: string,): Observable<MailBean> {
    return this.restService
      .get$<MailBean>(this.mailURL + '/send', {
        to: to,
        text: text,
        title: title
          ? title
          : "untitled",
      });
  }
}
