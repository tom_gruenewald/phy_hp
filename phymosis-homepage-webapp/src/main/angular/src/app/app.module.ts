import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {HeaderComponent} from './components/blocks/header/header.component';
import {NavigationComponent} from './components/blocks/navigation/navigation.component';
import {MainScrollContainerComponent} from './components/main-scroll-container/main-scroll-container.component';
import {NewsPageComponent} from './components/pages/news-page/news-page.component';
import {MusicPageComponent} from './components/pages/music-page/music-page.component';
import {LivePageComponent} from './components/pages/live-page/live-page.component';
import {ContactPageComponent} from './components/pages/contact-page/contact-page.component';
import {ImpressumFooterComponent} from './components/blocks/impressum-footer/impressum-footer.component';
import {HttpService} from "./services/http-service/http.service";
import { OutlineButtonComponent } from './components/basic/outline-button/outline-button.component';
import {SafePipe} from "./utils/SafePipe";
import {GoogleDriveLinkPipe} from "./utils/GoogledriveLinkPipe";
import {BandCampReleaseLinkPipe} from "./utils/BandCampReleaseLinkPipe";
import {BandCampSongLinkPipe} from "./utils/BandCampSongLinkPipe";
import { TourdateComponent } from './components/blocks/tourdate/tourdate.component';
import { TourdateVideoComponent } from './components/blocks/tourdate-video/tourdate-video.component';
import {GoogleMapsLinkPipe} from "./utils/GoogleMapsLinkPipe";
import { ContactFormComponent } from './components/blocks/contact-form/contact-form.component';
import { LoginFormComponent } from './components/blocks/login-form/login-form.component';
import { ContentEditDialogueComponent } from './components/blocks/content-edit-dialogue/content-edit-dialogue.component';

@NgModule({
  declarations: [
    GoogleMapsLinkPipe,
    BandCampSongLinkPipe,
    BandCampReleaseLinkPipe,
    GoogleDriveLinkPipe,
    SafePipe,
    AppComponent,
    HeaderComponent,
    NavigationComponent,
    MainScrollContainerComponent,
    NewsPageComponent,
    MusicPageComponent,
    LivePageComponent,
    ContactPageComponent,
    ImpressumFooterComponent,
    OutlineButtonComponent,
    TourdateComponent,
    TourdateVideoComponent,
    ContactFormComponent,
    LoginFormComponent,
    ContentEditDialogueComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
  ],
  exports: [],
  providers: [HttpService],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
  }
}
