package tgr.phymosis.homepage.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import tgr.phymosis.homepage.DatabaseConfig;

@Configuration
@ComponentScan(basePackageClasses = RestConfig.class)
@Import({DatabaseConfig.class})
public class RestConfig
{
  public static final String REST_PREFIX = "/rest/phy";
}
