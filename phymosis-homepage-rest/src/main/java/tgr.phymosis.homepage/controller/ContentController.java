package tgr.phymosis.homepage.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tgr.phymosis.homepage.beans.ContentBean;
import tgr.phymosis.homepage.beans.MediaLinkBean;
import tgr.phymosis.homepage.beans.SocialMediaLinkBean;
import tgr.phymosis.homepage.beans.TokenBean;
import tgr.phymosis.homepage.beans.enums.ContentOrderType;
import tgr.phymosis.homepage.beans.enums.ContentType;
import tgr.phymosis.homepage.config.RestConfig;
import tgr.phymosis.homepage.service.ContentService;
import tgr.phymosis.homepage.service.MediaLinkService;
import tgr.phymosis.homepage.service.SocialMediaLinkService;

import java.util.ArrayList;
import java.util.List;

/**
 * offers all content related data, like media links and posts
 */
@RestController
@RequestMapping(RestConfig.REST_PREFIX + "/content")
public class ContentController
{

  private final ContentService contentService;
  private final MediaLinkService mediaLinkService;
  private final SocialMediaLinkService socialMediaLinkService;

  public ContentController(ContentService contentService,
                           MediaLinkService mediaLinkService, SocialMediaLinkService socialMediaLinkService)
  {
    this.contentService = contentService;
    this.mediaLinkService = mediaLinkService;
    this.socialMediaLinkService = socialMediaLinkService;
  }

  @GetMapping("getPageByType")
  public ResponseEntity<List<ContentBean>> getPageByType(@RequestParam("type") ContentType contentType,
                                                         @RequestParam("page") Integer page,
                                                         @RequestParam("size") Integer size,
                                                         @RequestParam("orderType") ContentOrderType contentOrderType,
                                                         @RequestParam("active") Boolean active)
  {
    List<ContentBean> contentBeans = this.contentService.getContentPageableByType(contentType,
                                                                                  page,
                                                                                  size,
                                                                                  contentOrderType,
                                                                                  active);

    return contentBeans != null
           ? ResponseEntity.ok(contentBeans)
           : ResponseEntity.ok(new ArrayList<ContentBean>());
  }

  @GetMapping("all")
  public ResponseEntity<List<ContentBean>> getAll()
  {
    List<ContentBean> contentBeans = this.contentService.getAllContent();

    return contentBeans != null
           ? ResponseEntity.ok(contentBeans)
           : ResponseEntity.ok(new ArrayList<>());
  }

  @GetMapping("media/getByContentId")
  public ResponseEntity<List<MediaLinkBean>> getMediaByContentId(@RequestParam("contentId") Long contentId)
  {
    List<MediaLinkBean> mediaLinkBeans = this.mediaLinkService.getMediaLinksByContentId(contentId);

    return mediaLinkBeans != null
           ? ResponseEntity.ok(mediaLinkBeans)
           : ResponseEntity.ok(new ArrayList<>());
  }

  @GetMapping("getSocialLinkByContentId")
  public ResponseEntity<SocialMediaLinkBean> getSocialLinkByContentId(@RequestParam("contentId") Long contentId)
  {
    SocialMediaLinkBean socialMediaLinkBean = socialMediaLinkService.getSocialMediaLinkByContentId(contentId);
    return ResponseEntity.ok(socialMediaLinkBean);
  }

  @GetMapping("number")
  public ResponseEntity<Integer> getContentNumber(@RequestParam("type") ContentType contentType)
  {
    return ResponseEntity.ok(this.contentService.getNumberOfContentByType(contentType));
  }

  @GetMapping("numberActive")
  public ResponseEntity<Integer> getActiveContentNumber(@RequestParam("type") ContentType contentType)
  {
    return ResponseEntity.ok(this.contentService.getNumberOfActiveContentByType(contentType));
  }

  @GetMapping("deactivate")
  public ResponseEntity<Boolean> deactivateContentById(@RequestParam("contentId") Long contentId,
                                                       @RequestParam("token") String token)
  {
    return ResponseEntity.ok(this.contentService.deactivateContentById(contentId, new TokenBean(token)));
  }

  @GetMapping("activate")
  public ResponseEntity<Boolean> reactivateContentById(@RequestParam("contentId") Long contentId,
                                                       @RequestParam("token") String token)
  {
    return ResponseEntity.ok(this.contentService.reactivateContentById(contentId, new TokenBean(token)) != null);
  }
}
