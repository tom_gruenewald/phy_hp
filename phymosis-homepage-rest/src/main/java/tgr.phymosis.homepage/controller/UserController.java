package tgr.phymosis.homepage.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import tgr.phymosis.homepage.config.RestConfig;
import tgr.phymosis.homepage.service.UserService;

/**
 * Offers Services to get user information
 */
@RestController
@RequestMapping(RestConfig.REST_PREFIX + "/user")
public class UserController {
  private final UserService userService;

  public UserController(UserService userService)
  {
    this.userService = userService;
  }

}
