package tgr.phymosis.homepage.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tgr.phymosis.homepage.beans.TokenBean;
import tgr.phymosis.homepage.beans.UserBean;
import tgr.phymosis.homepage.config.RestConfig;
import tgr.phymosis.homepage.service.TokenService;
import tgr.phymosis.homepage.service.UserService;

/**
 * Serves the Administrators user authentication as well as requesting new tokens
 */
@RestController
@RequestMapping(RestConfig.REST_PREFIX + "/auth")
public class AuthenticationController
{

  private final UserService userService;
  private final TokenService tokenService;

  public AuthenticationController(UserService userService, TokenService tokenService)
  {
    this.userService = userService;
    this.tokenService = tokenService;
  }

  @GetMapping("login")
  public ResponseEntity<UserBean> login(@RequestParam("name") @NonNull String name,
                                        @RequestParam("password") @NonNull String password)
  {
    UserBean userBean = this.userService.getUserByNameAndPassword(name, password);
    TokenBean tokenBean = this.tokenService.createToken(name, password);
    if(tokenBean != null)
    {
      userBean.setTokenBean(tokenBean);
    }
    return ResponseEntity.ok(userBean);
  }

  @GetMapping("logout")
  public void logout(@RequestParam("userId") @NonNull Long userId)
  {
    this.tokenService.deleteTokensByUserId(userId);
  }
}
