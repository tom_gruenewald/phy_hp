package tgr.phymosis.homepage.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tgr.phymosis.homepage.beans.MailBean;
import tgr.phymosis.homepage.config.RestConfig;
import tgr.phymosis.homepage.service.MailService;

@RestController
@RequestMapping(RestConfig.REST_PREFIX + "/mail")
public class MailController {

  private final String currentEmail = "phymosis-homepage-notifier@gmx.de";

  public final MailService mailService;

  public MailController(MailService mailService)
  {
    this.mailService = mailService;
  }

  @GetMapping("send")
  public ResponseEntity<MailBean> sendMail(@RequestParam("to") @NonNull String to,
                                           @RequestParam("text")  @Nullable String text,
                                           @RequestParam("title") @Nullable String title)
  {
    return ResponseEntity.ok(this.mailService.sendEmail(currentEmail, to, title, text));
  }
}
