package tgr.phymosis.homepage.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import tgr.phymosis.homepage.beans.TourdateBean;
import tgr.phymosis.homepage.config.RestConfig;
import tgr.phymosis.homepage.service.TourdateService;

import java.util.List;

/**
 * provides tourdate related data
 */
@RestController
@RequestMapping(RestConfig.REST_PREFIX + "/tourDates")
public class TourdateController
{

  private final TourdateService tourdateService;

  public TourdateController(TourdateService tourdateService)
  {
    this.tourdateService = tourdateService;
  }

  @GetMapping("getUpcomingTourDatesPage")
  public ResponseEntity<List<TourdateBean>> getUpcomingTourDatesPage(@RequestParam("page") Integer page,
                                                                     @RequestParam("size") Integer size)
  {
    return ResponseEntity.ok(this.tourdateService.getUpcomingTourDatesPage(page, size));
  }

  @GetMapping("getPastTourDatesPage")
  public ResponseEntity<List<TourdateBean>> getPastTourDatesPage(@RequestParam("page") Integer page,
                                                                 @RequestParam("size") Integer size)
  {
    return ResponseEntity.ok(this.tourdateService.getPastTourDatesPage(page, size));
  }
}
